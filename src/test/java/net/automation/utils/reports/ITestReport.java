package net.automation.utils.reports;

import com.aventstack.extentreports.ExtentTest;
import org.testng.ITestResult;

public interface ITestReport {
    void stepPass(String step);
    void stepFail(String step);
    void addInfo(String info);

    ExtentTest getTest();


    void conclude(ITestResult result, ITestReport currentTestReport);
}
