package net.automation.utils.reports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class ExtentReport implements IReport {

    private ExtentReports extent;
    private String reportNameJenkins = "";
    private static final String buildNumber = System.getenv("BUILD_NUMBER");
    private static String reportPath = "";

    public ExtentReport() {
    }

    public static ExtentReport getInstance() {
        return ExtentReport.InstanceHolder.INSTANCE;
    }

    public ExtentReport(String reportName) {
        reportPath = System.getProperty("user.dir") + "/Reporter/reports/" + reportName + "/" + reportName + ".html";

        reportNameJenkins = reportName;

        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(reportPath);

        htmlReporter.config().setDocumentTitle("Automation Report");
        htmlReporter.config().setReportName(reportName);
        htmlReporter.config().setTheme(Theme.DARK);
        htmlReporter.config().setTimeStampFormat("dd/MM/yyyy HH:mm:ss");
        htmlReporter.config().setCSS(".r-img { width: 30%; }");


        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        //Add a Jenkins job number in the report ONLY if the project is built from Jenkins
        if (buildNumber != null) {
            extent.setSystemInfo("Jenkins Job #", "<a href='http://localhost:8080/job/AutomationSystem/" + "'>" + buildNumber + "</a>");
        }
    }

    @Override
    public ITestGroupReport createNewTestGroup(String testGroupName) {
        return new ExtentTestGroup(extent.createTest(testGroupName));
    }

    public void removeTest(ExtentTest test) {
        extent.removeTest(test);
    }

    @Override
    public void setSystemInfo(String key, String value) {
        extent.setSystemInfo(key, value);
    }

    public void generate() {
        extent.flush();

        //create a file copy for the Jenkins run ONLY if the project is built from Jenkins
        if (buildNumber != null) {
            copyTestForJenkinsBuild();
        }
    }

    public void copyTestForJenkinsBuild() {
        File reportSource = new File(System.getProperty("user.dir") + "/Reporter/reports/" + File.separator + reportNameJenkins + "/" + File.separator + reportNameJenkins + ".html");
        File reportDestination = new File(System.getProperty("user.dir") + "/Reporter/reports/Jenkins/" + File.separator + "Jenkins Extent Report.html");

        try {
            Files.copy(Paths.get(String.valueOf(reportSource)), Paths.get(String.valueOf(reportDestination)), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getReportPath() {
        return reportPath;
    }

    private static final class InstanceHolder {
        private static final ExtentReport INSTANCE = new ExtentReport();
    }
}