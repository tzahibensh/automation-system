package net.automation.utils.reports;

import com.aventstack.extentreports.ExtentTest;

public class ExtentTestGroup implements ITestGroupReport {

    private final ExtentTest extentTest;

    ExtentTestGroup(ExtentTest extentTest){
        this.extentTest = extentTest;
    }

    @Override
    public ITestReport addTest(String name) {
        return new ExtentTestReport(extentTest.createNode(name));
    }
}
