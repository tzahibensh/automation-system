package net.automation.utils.reports;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.Status;
import net.automation.utils.listeners.RetryAnalyzer;
import net.automation.utils.ScreenShot;
import org.testng.ITestResult;

import java.io.IOException;

public class ExtentTestReport implements ITestReport {

    private final ExtentTest extentTest;
    public static String resultMessage = "";

    public ExtentTestReport(ExtentTest extentTest) {
        this.extentTest = extentTest;
    }

    @Override
    public void stepPass(String step) {
        extentTest.pass(step);
    }

    @Override
    public void stepFail(String step) {
        extentTest.fail(step);
    }

    @Override
    public void addInfo(String info) {
        extentTest.info(info);
    }

    @Override
    public ExtentTest getTest() {
        return extentTest;
    }

    public void conclude(ITestResult result, ITestReport currentTestReport) {
        currentTestReport.addInfo("Number of iterations: " + RetryAnalyzer.getInstance().getIterationNumber());
        switch (result.getStatus()) {
            case ITestResult.FAILURE:
                extentTest.log(Status.FAIL, result.getThrowable().getMessage(), getScreenshot());
                resultMessage = result.getThrowable().getMessage();
                break;
            case ITestResult.SUCCESS:
                if (RetryAnalyzer.getInstance().getIterationNumber() > 1) {
                    extentTest.log(Status.WARNING, "Test passed");
                } else {
                    extentTest.log(Status.PASS, "Test passed, on the " + RetryAnalyzer.getInstance().getIterationNumber() + " iteration");
                }
                break;
            case ITestResult.SKIP:
                extentTest.log(Status.SKIP, Status.SKIP.toString());
                break;
        }
    }

    private MediaEntityModelProvider getScreenshot() {
        if (!System.getProperty("testType").equals("api") && RetryAnalyzer.getInstance().isRetriesCounterAchievedMaxRetries()) {
            try {
                extentTest.addScreenCaptureFromPath(ScreenShot.getInstance().getScreenShotPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return ScreenShot.getInstance().getWebPageScreenShot();
        }
        return null;
    }
}