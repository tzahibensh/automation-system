package net.automation.utils.reports;

import com.aventstack.extentreports.ExtentTest;

import java.io.IOException;

public interface IReport {
    ITestGroupReport createNewTestGroup(String testGroupName);

    void generate();

    void copyTestForJenkinsBuild() throws IOException;

    void removeTest(ExtentTest test);

    void setSystemInfo(String key, String value);
}
