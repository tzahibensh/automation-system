package net.automation.utils.reports;


public interface ITestGroupReport {

    ITestReport addTest(String name);
}
