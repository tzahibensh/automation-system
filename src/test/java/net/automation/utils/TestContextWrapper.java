package net.automation.utils;

import org.apache.commons.lang3.StringUtils;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;

import java.lang.reflect.Method;
import java.util.List;

public class TestContextWrapper {

    public TestContextWrapper() {
    }

    public static TestContextWrapper getInstance() {
        return TestContextWrapper.InstanceHolder.INSTANCE;
    }

    private static final String DEFAULT_SUITE_NAME = "default suite";
    private static ITestContext context;
    private String testName = "";

    public TestContextWrapper(ITestContext context) {
        this.context = context;
    }

    public ITestContext getContext() {
        return context;
    }

    public boolean isDefaultSuite() {
        return getSuiteNameImpl(context).toLowerCase().equals(DEFAULT_SUITE_NAME);
    }

    public String getSuiteName() {
        return getSuiteNameImpl(context);
    }

    public String getTestGroupName() {
        List<ITestNGMethod> allMethods = context.getSuite().getAllMethods();
        String testName = "";

        // TODO: 8/21/2019 refactor this?
        for (ITestNGMethod method : allMethods) {
            String fullMethod = method.toString();
            int indexOf = fullMethod.indexOf(".");
            testName = StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(fullMethod.replace(fullMethod.substring(indexOf), "").replaceAll("\\d+", "")), " ").toLowerCase();
        }

        return StringUtils.capitalize(testName);
    }

    private String getSuiteNameImpl(ITestContext context) {
        return context.getSuite().getName();
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(Method method) {
        this.testName = Utils.capitalizeAndAddSpaces(method.getName());
    }

    private static final class InstanceHolder {
        private static final TestContextWrapper INSTANCE = new TestContextWrapper();
    }
}