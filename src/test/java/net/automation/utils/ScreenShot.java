package net.automation.utils;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import net.automation.sut.tests.screen.api.android.Android;
import net.automation.utils.listeners.RetryAnalyzer;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import javax.annotation.Nullable;

import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class ScreenShot {
    private String destinationPath = "";

    public static ScreenShot getInstance() {
        return ScreenShot.InstanceHolder.INSTANCE;
    }

    public void screenShotManager(@Nullable WebDriver driver, String reportName) {
        if (RetryAnalyzer.getInstance().isRetriesCounterAchievedMaxRetries()) {
            if (System.getProperty("testType").equals("web")) {
                takeWebPageScreenShot(driver, reportName);
            } else {
                takeAppScreenShot(reportName);
            }
        }
    }

    private void takeWebPageScreenShot(WebDriver driver, String reportName) {
        TakesScreenshot ts = (TakesScreenshot) driver;
        moveScreenShotFileToReportFolder(reportName, ts.getScreenshotAs(OutputType.FILE));
    }

    public MediaEntityModelProvider getWebPageScreenShot() {
        MediaEntityModelProvider screenShot = null;
        try {
            screenShot = MediaEntityBuilder.createScreenCaptureFromPath(destinationPath).build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return screenShot;
    }

    public String getScreenShotPath() {
        return destinationPath;
    }

    private void takeAppScreenShot(String reportName) {
        reportName = reportName.replace(" ", "_");
        System.out.println("sdcard/" + reportName + ".png");
        Android.adb.tackScreenShot("sdcard/" + reportName + ".png");
        File source = new File(System.getProperty("user.dir") + "/Reporter/reports/" + reportName + ".png");
        Android.adb.pullFile("sdcard/" + reportName + ".png", source.toString());
        reportName = reportName.replace("_", " ");
        moveScreenShotFileToReportFolder(reportName, source);
    }

    private void moveScreenShotFileToReportFolder(String reportName, File source) {
        destinationPath = System.getProperty("user.dir") + "/Reporter/reports/" + reportName + "/" + reportName + ".png";
        File destination = new File(destinationPath);
        try {
            FileUtils.moveFile(source, destination);
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    private static final class InstanceHolder {
        private static final ScreenShot INSTANCE = new ScreenShot();
    }
}
