package net.automation.utils.driverManager;

import java.io.*;
import java.util.ArrayList;

public class GridSupport {

    private static void createGridBatchFile(String fileName, String fileContent) {
        String line;
        ArrayList<String> commandResponse = new ArrayList<>();

        try {
            //final File file = new File("C:\\Users\\USER\\IdeaProjects\\AutomationSystem\\" + fileName + ".bat");

            /*PrintWriter writer = new PrintWriter(file, "UTF-8");
            writer.println(fileContent);
            writer.close();*/

            //Process p = Runtime.getRuntime().exec("cmd.exe /c " + "C:\\Users\\USER\\IdeaProjects\\AutomationSystem\\Grid\\" + fileName + ".bat");
            //p.waitFor();
            ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", " start C:\\Users\\USER\\IdeaProjects\\AutomationSystem\\Grid\\" + fileName + ".bat");
            builder.redirectErrorStream(true);
            Process p = builder.start();
            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while (true) {
                line = r.readLine();
                if (line == null) {
                    break;
                } else {
                    System.out.println(line);
                    commandResponse.add(line);
                }
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void startHub() {
        createGridBatchFile("hub", "java -jar selenium-server-standalone-3.141.59.jar -role hub");
    }
}
