package net.automation.utils.driverManager;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import net.automation.sut.utils.ADB;
import net.automation.sut.utils.MyLogger;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.ArrayList;

import static org.testng.Assert.assertTrue;

public class AppiumSupport {

    protected static ArrayList<String> getAvailableDevices() {
        //connected devices that are available for testing
        ArrayList<String> availableDevices = new ArrayList<>();

        //get all the connected devices through ADB command
        ArrayList<String> connectedDevices = removeEmulators(ADB.getConnectedDevices());

        for (String device : connectedDevices) {
            //initialize if test finished successfully in the previous run
            DriverSupport.setIsTestFinishedSuccessfully(device);

            //updating the DB with 'false' value for Is test finished successfully
            DriverSupport.updateIsTestFinishedSuccessfullyInDb(device);

            //get all apps installed in the device
            ArrayList<String> apps = new ADB(device).getInstalledPackages();

            //add available device
            addDeviceToAvailableDevicesList(device, apps, availableDevices);
        }
        //if there are no devices connected, throw exception
        if (connectedDevices.size() == 0) {
            throw new RuntimeException("No devices available for testing");
        }
        return availableDevices;
    }

    protected static void addDeviceToAvailableDevicesList(String device, ArrayList<String> apps, ArrayList<String> availableDevices) {
        if (!DriverSupport.isTestFinishedSuccessfully() && DriverSupport.isDeviceContainsApp(apps)) {
            MyLogger.log.info("Device: " + device + " has " + DriverSupport.getAppiumSettingsAppPackage() + " installed, and previous run did not finish successfully, uninstalling the app");
            uninstallAppiumSettingsApp();
            availableDevices.add(device);
        } else if (DriverSupport.isTestFinishedSuccessfully() && DriverSupport.isDeviceContainsApp(apps)) {
            MyLogger.log.info("Device: " + device + " has " + DriverSupport.getAppiumSettingsAppPackage() + " installed, assuming it is under testing");
        } else {
            availableDevices.add(device);
        }
    }

    public static void startServer() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("noReset", "false");

        //Build the Appium service
        AppiumServiceBuilder builder = new AppiumServiceBuilder();
        builder.withIPAddress("0.0.0.0");
        builder.usingPort(DriverSupport.getAppiumURLPort());
        builder.withCapabilities(capabilities);
        builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
        builder.withArgument(GeneralServerFlag.LOG_LEVEL, "error");
        builder.withArgument(GeneralServerFlag.RELAXED_SECURITY);

        //Start the server with the builder
        DriverSupport.setService(AppiumDriverLocalService.buildService(builder));
        if (DriverSupport.getService().isRunning()) {
            MyLogger.log.info("Service was already running on url: " + DriverSupport.getService().getUrl().toString());
        } else {
            DriverSupport.getService().start();
            MyLogger.log.info("Service started on url: " + DriverSupport.getService().getUrl().toString());
            assertTrue(DriverSupport.getService().isRunning(), "Unable to start service, closing");
        }
    }

    public static void uninstallAppiumSettingsApp() {
        ADB.uninstallApp(DriverSupport.getDeviceId(), DriverSupport.getAppiumSettingsAppPackage());
    }

    static ArrayList<String> removeEmulators(ArrayList<String> connectedEmulators) {
        connectedEmulators.removeIf(device -> device.contains("emulator"));
        return connectedEmulators;
    }
}