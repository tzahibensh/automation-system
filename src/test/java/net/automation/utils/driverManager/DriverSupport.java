package net.automation.utils.driverManager;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import net.automation.sut.tests.screen.api.android.Android;
import net.automation.sut.utils.*;
import net.automation.utils.TestContextWrapper;
import net.automation.sut.utils.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class DriverSupport {

    private static final String DEFAULT_SUITE_NAME = "default suite";
    private static final String SPEEDTEST_SUITE = "speedtestsuite";
    private static HashMap<String, URL> hosts;
    private static int appiumURLPort;
    private final static String appiumSettingsAppPackage = "io.appium.settings";
    private static String cell;
    static boolean isTestFinishedSuccessfully;
    private static AppiumDriverLocalService service;
    private static String deviceId;
    final static String portsInUse = "netstat -ano | findstr ";

    public static void setDeviceId(String deviceId) {
        DriverSupport.deviceId = deviceId;
    }

    protected static URL host(String deviceId) throws MalformedURLException {
        //creating host for appium
        if (hosts == null) {
            hosts = new HashMap<>();
            hosts.put(deviceId, new URL("http://0.0.0.0:" + appiumURLPort + "/wd/hub"));
        }
        return hosts.get(deviceId);
    }

    public static String getDeviceId() {
        return deviceId;
    }

    public static String getAppiumSettingsAppPackage() {
        return appiumSettingsAppPackage;
    }

    public static void changeAppState(Command command) {
        String suiteName = TestContextWrapper.getInstance().getSuiteName().toLowerCase();
        switch (suiteName) {
            case DEFAULT_SUITE_NAME:
            case SPEEDTEST_SUITE:
                if (command.toString().toLowerCase().equals(Command.OPEN.command.toLowerCase())) {
                    Android.app.speedTest.open();
                } else if (command.toString().toLowerCase().equals(Command.CLOSE.command.toLowerCase())) {
                    Android.app.speedTest.forceStop();
                }
                break;
        }
    }

    public static void closeApps(String device) {
        ADB adb = new ADB(device);
        switch (adb.getDeviceModel().toLowerCase()) {
            case "redmi note 5":
                Android.adb.openAppSwitch();
                while (!Android.app.androidOS.isNoRecentItemsExist() && !Android.app.androidOS.isAppIconVisible()) {
                    Android.adb.selectAppInAppSwitch();
                    Android.adb.closeAppInAppSwitch();
                }
                Android.adb.showAndroidHome();
        }
    }

    public enum Command {
        OPEN("open"),
        CLOSE("close");

        private final String command;

        Command(String command) {
            this.command = command;
        }

        public String getCommand() {
            return command;
        }
    }

    protected static void setIsTestFinishedSuccessfully(String device) {
        isTestFinishedSuccessfully = getValueFromDb("ProjectParameters!A2:A10", "ProjectParameters!B2:B10", device).equals("true");
    }

    public static boolean checkIfServerIsRunning() {

        boolean isServerRunning = false;
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(appiumURLPort);
            serverSocket.close();
        } catch (IOException e) {
            //If control comes here, then it means that the port is in use
            isServerRunning = true;
        } finally {
            serverSocket = null;
        }
        return isServerRunning;
    }

    public static void setPort() {
        appiumURLPort = Integer.parseInt(getValueFromDb("ProjectParameters!A2:A10", "ProjectParameters!C2:C10", deviceId));
        MyLogger.log.info("Setting port: " + appiumURLPort);
    }

    public static int getAppiumURLPort() {
        return appiumURLPort;
    }

    static String getRunningPortId(String command) {
        StringBuilder port = new StringBuilder();
        String[] commandResponse = RunCommand.runCommand(command).split("");
        for (int i = commandResponse.length - 1; i > 0; i--) {
            if (commandResponse[i].equals(" ")) {
                break;
            } else {
                port.append(commandResponse[i]);
            }
        }
        return new StringBuilder(port.toString()).reverse().toString();
    }

    static boolean isDeviceContainsApp(ArrayList<String> apps) {
        return apps.contains("package:" + appiumSettingsAppPackage);
    }

    public static String getValueFromDb(String keysRange, String valuesRange, String device) {
        return GoogleAPIs.getValuesOfCellsInRange(valuesRange).get(GoogleAPIs.getValuesOfCellsInRange(keysRange).indexOf(device));
    }

    protected static boolean isTestFinishedSuccessfully() {
        return isTestFinishedSuccessfully;
    }

    protected static void updateIsTestFinishedSuccessfullyInDb(String key) {
        String cellToUpdate = String.valueOf("ProjectParameters!B2:B4".charAt("ProjectParameters!B2:B4".indexOf("!") + 1));
        cellToUpdate += Integer.toString(GoogleAPIs.getValuesOfCellsInRange("ProjectParameters!A2:A4").indexOf(key) + 2);
        GoogleAPIs.writeToCell(cellToUpdate, "false");
        cell = cellToUpdate;
    }

    public static String getCell() {
        return cell;
    }

    static AppiumDriverLocalService getService(){
        return service;
    }

    static void setService(AppiumDriverLocalService service){
        DriverSupport.service = service;
    }

    static String getPortInUse(){
        return portsInUse;
    }

    public static void stopServer() {
        MyLogger.log.info("Stopping server on port " + getAppiumURLPort());

        String portId = getRunningPortId(DriverSupport.getPortInUse() + getAppiumURLPort());
        if (GeneralCommands.killTask(portId).equals("SUCCESS: The process with PID " + portId + " has been terminated.")) {
            MyLogger.log.info("Appium server at port " + portId + " was terminated");
        }
        if (checkIfServerIsRunning()) {
            DriverSupport.getService().stop();
        }
    }
}
