package net.automation.utils.driverManager;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import net.automation.sut.tests.screen.api.android.Android;
import net.automation.sut.tests.webpage.BaseWebPage;
import net.automation.sut.tests.webpage.Web;
import net.automation.sut.utils.ADB;
import net.automation.sut.utils.GeneralCommands;
import net.automation.sut.utils.GoogleAPIs;
import net.automation.sut.utils.MyLogger;
import net.automation.tests.AbstractTest;
import net.automation.utils.RuntimeSettingsManager;
import net.automation.utils.Utils;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.*;

public class DriverManager {

    private static final String DESKTOP_CHROME = "desktopChrome";
    private static final String ANDROID_CHROME = "androidChrome";
    private static final String ANDROID_NATIVE_APP = "androidNativeApp";
    private static final String EMULATOR_CHROME = "emulatorChrome";

    public static DriverManager getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public void createDriver(@Nonnull String executionType) throws IOException {
        switch (executionType) {
            case DESKTOP_CHROME:
                MyLogger.log.info("Starting " + DESKTOP_CHROME + " driver");

                ChromeOptions desktopChromeOptions = new ChromeOptions();
                desktopChromeOptions.setCapability("platform", "windows");
                System.setProperty("webdriver.chrome.driver", "C:\\Users\\tzahi\\IdeaProjects\\automation-system\\drivers\\chromedriver.exe");

                Web.remoteWebDriver.set(new RemoteWebDriver(new URL("http://" + GridManager.getGridNodeConfig().host + ":" + GridManager.getGridNodeConfig().port + "/wd/hub"), desktopChromeOptions));
                assertNotNull(Web.remoteWebDriver, "Chrome web driver is null");

                Web.remoteWebDriver.get().manage().window().maximize();
                Web.remoteWebDriver.get().manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
                Web.remoteWebDriver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                BaseWebPage.navigateToUrl(RuntimeSettingsManager.getBaseUrl());
                break;

            case ANDROID_CHROME:
                MyLogger.log.info("Starting " + ANDROID_CHROME + " driver");
                ArrayList<String> androidChromeDevices;

                androidChromeDevices = AppiumSupport.getAvailableDevices();
                for (String device : androidChromeDevices) {
                    MyLogger.log.info("Trying to create new web driver for " + device);

                    Web.adb = new ADB(device);
                    assertNotNull(Web.adb, "ADB wasn't initialized properly");

                    String deviceName = Web.adb.getDeviceName() + ", " + device;
                    String androidVersion = Web.adb.getAndroidVersionAsString();
                    DesiredCapabilities AndroidChromeCapabilities = new DesiredCapabilities();

                    AndroidChromeCapabilities.setCapability("chromedriverExecutableDir", "C:\\Users\\USER\\IdeaProjects\\AutomationSystem\\drivers\\");
                    AndroidChromeCapabilities.setCapability("chromedriverChromeMappingFile", "https://chromedriver.storage.googleapis.com/");
                    AndroidChromeCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
                    AndroidChromeCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
                    AndroidChromeCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, androidVersion);
                    AndroidChromeCapabilities.setCapability(MobileCapabilityType.BROWSER_NAME, MobileBrowserType.CHROME);

                    AbstractTest.report.setSystemInfo("Execution Type", Utils.capitalizeAndAddSpaces(MobileBrowserType.CHROME) + ", Real device");
                    AbstractTest.report.setSystemInfo(Utils.capitalizeAndAddSpaces(MobileCapabilityType.DEVICE_NAME), deviceName);
                    AbstractTest.report.setSystemInfo(Utils.capitalizeAndAddSpaces(MobileCapabilityType.PLATFORM_VERSION), androidVersion);

                    DriverSupport.setDeviceId(device);
                    DriverSupport.setPort();
                    AppiumSupport.startServer();
                    Web.driver = new RemoteWebDriver(DriverSupport.host(device), AndroidChromeCapabilities);
                    assertNotNull(Web.driver, "Android driver wasn't initialized properly");
                }

                break;
            case ANDROID_NATIVE_APP:
                MyLogger.log.info("Starting " + ANDROID_NATIVE_APP + " driver");

                ArrayList<String> androidNativeAppDevices;

                androidNativeAppDevices = AppiumSupport.getAvailableDevices();
                //initializing adb and android driver for each device
                for (String device : androidNativeAppDevices) {
                    MyLogger.log.info("Trying to create new driver for " + device);

                    Android.adb = new ADB(device);
                    assertNotNull(Android.adb, "ADB wasn't initialized properly");
                    //Android.adb.unlockDevice();

                    String deviceName = Android.adb.getDeviceName() + ", " + device;
                    String androidVersion = Android.adb.getAndroidVersionAsString();
                    DesiredCapabilities androidNativeAppCapabilities = new DesiredCapabilities();

                    androidNativeAppCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName + ", " + device);
                    androidNativeAppCapabilities.setCapability(AndroidMobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
                    androidNativeAppCapabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
                    androidNativeAppCapabilities.setCapability("automatorName", "UiAutomator2");
                    androidNativeAppCapabilities.setCapability(AndroidMobileCapabilityType.NO_SIGN, true);

                    AbstractTest.report.setSystemInfo("Execution Type", "Native app " + deviceName + ", Real device");
                    AbstractTest.report.setSystemInfo(Utils.capitalizeAndAddSpaces(MobileCapabilityType.DEVICE_NAME), deviceName);
                    AbstractTest.report.setSystemInfo(Utils.capitalizeAndAddSpaces(MobileCapabilityType.PLATFORM_VERSION), androidVersion);

                    DriverSupport.closeApps(device);
                    DriverSupport.setDeviceId(device);
                    DriverSupport.setPort();
                    AppiumSupport.startServer();

                    Android.driver = new AndroidDriver<>(DriverSupport.host(device), androidNativeAppCapabilities);
                    assertNotNull(Android.driver, "Android driver wasn't initialized properly");
                    DriverSupport.changeAppState(DriverSupport.Command.OPEN);
                }

                break;
            case EMULATOR_CHROME:
                MyLogger.log.info("Starting emulator driver");
                for (String device : GeneralCommands.getConnectedEmulatorDevices()) {
                    EmulatorSupport.startEmulator(device);

                    ChromeOptions chromeOptions = new ChromeOptions();
                    chromeOptions.setCapability(MobileCapabilityType.DEVICE_NAME, device);
                    chromeOptions.setCapability(MobileCapabilityType.NO_RESET, "true");
                    chromeOptions.setCapability(MobileCapabilityType.FULL_RESET, "false");
                    chromeOptions.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
                    chromeOptions.setCapability("avd", device);

                    AbstractTest.report.setSystemInfo("Execution Type", "Native app " + device + ", Real device");
                    AbstractTest.report.setSystemInfo(Utils.capitalizeAndAddSpaces(MobileCapabilityType.DEVICE_NAME), device);

                    DriverSupport.setDeviceId(device);
                    DriverSupport.setPort();
                    EmulatorSupport.startEmulatorServer();
                    Web.remoteWebDriver.set(new RemoteWebDriver(DriverSupport.host(device), chromeOptions));
                    //Web.driver = new RemoteWebDriver(DriverSupport.host(device), chromeOptions);

                    Web.remoteWebDriver.get().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                    BaseWebPage.navigateToUrl(RuntimeSettingsManager.getBaseUrl());
                }
                break;
        }
    }

    public void terminateDriver(@Nonnull String executionType) {
        MyLogger.log.info("Terminating run");
        switch (executionType) {
            case DESKTOP_CHROME:
                Web.remoteWebDriver.get().quit();

                break;
            case ANDROID_CHROME:
                Web.driver.quit();

                //Web.adb.uninstallApp(appiumSettingsAppPackage);
                Web.adb.closeScreen();

                DriverSupport.stopServer();

                break;
            case ANDROID_NATIVE_APP:
                Android.driver.quit();

                DriverSupport.changeAppState(DriverSupport.Command.CLOSE);
                AppiumSupport.uninstallAppiumSettingsApp();
                Android.adb.closeScreen();

                GoogleAPIs.writeToCell("ProjectParameters!" + DriverSupport.getCell(), "true");

                DriverSupport.stopServer();

                break;

            case EMULATOR_CHROME:
                Web.remoteWebDriver.get().quit();

                //TODO fix emulator uninstall settings app
                //EmulatorSupport.uninstallEmulatorAppiumSettingsApp(EmulatorSupport.getEmulatorPortId());

                Web.adb.closeScreen();

                GoogleAPIs.writeToCell("ProjectParameters!" + DriverSupport.getCell(), "true");

                DriverSupport.stopServer();
        }
    }

    private static final class InstanceHolder {
        private static final DriverManager INSTANCE = new DriverManager();
    }
}