package net.automation.utils.driverManager;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import net.automation.sut.utils.ADB;
import net.automation.sut.utils.GeneralCommands;
import net.automation.sut.utils.MyLogger;
import net.automation.sut.utils.RunCommand;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.ArrayList;

public class EmulatorSupport {
    private static AppiumDriverLocalService service;
    static String emulatorPortId;

    protected static ArrayList<String> getAvailableEmulatorDevices() {
        //connected devices that are available for testing
        ArrayList<String> availableEmulatorDevices = new ArrayList<>();

        //get all the connected emulator devices through ADB command
        //ArrayList<String> connectedDevicesByName = GeneralCommands.getConnectedEmulatorDevices();
        ArrayList<String> connectedEmulators = removeNonEmulators(ADB.getConnectedDevices());

        for (String connectedEmulator : connectedEmulators) {

            EmulatorSupport.setEmulatorPortId(connectedEmulator);

            String emulatorName = GeneralCommands.getEmulatorNameByPort(connectedEmulator);

            //initialize if test finished successfully in the previous run
            DriverSupport.setIsTestFinishedSuccessfully(emulatorName);

            //updating the DB with 'false' value for 'Is test finished successfully'
            DriverSupport.updateIsTestFinishedSuccessfullyInDb(emulatorName);

            //get all apps installed in the device
            ArrayList<String> apps = new ADB(connectedEmulator).getInstalledPackages();

            //add available device
            addEmulatorToAvailableEmulatorsList(emulatorName, connectedEmulator, apps, availableEmulatorDevices);
        }
        //if there are no devices connected, throw exception
        if (connectedEmulators.size() == 0 || availableEmulatorDevices.size() == 0) {
            throw new RuntimeException("No devices available for testing");
        }
        //return availableEmulatorDevices;
        return availableEmulatorDevices;
    }

    public static void startEmulator(String emulatorDevice) {
        Thread emulator = new Thread(() -> RunCommand.runCommand("emulator -avd " + emulatorDevice));
        emulator.setName(emulatorDevice);
        emulator.start();
    }

    public static void startEmulatorServer() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("noReset", "false");

        AppiumServiceBuilder builder = new AppiumServiceBuilder();
        builder.withIPAddress("0.0.0.0");
        builder.usingPort(DriverSupport.getAppiumURLPort());
        builder.withCapabilities(capabilities);
        builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
        builder.withArgument(GeneralServerFlag.LOG_LEVEL, "error");
        builder.withArgument(GeneralServerFlag.RELAXED_SECURITY);
        service = AppiumDriverLocalService.buildService(builder
                .withArgument(() -> "--avd", DriverSupport.getDeviceId()));
        service.start();
    }

    public static ArrayList<String> getAvailableEmulatorsPorts() {
        ArrayList<String> emulatorPorts = new ArrayList<>();
        ArrayList<String> devices = ADB.getConnectedDevices();
        for (String device : devices) {
            if (device.contains("emulator")) {
                emulatorPorts.add(device);
            }
        }
        return emulatorPorts;
    }

    public static void uninstallEmulatorAppiumSettingsApp(String emulatorPort) {
        ADB.uninstallApp(emulatorPort, DriverSupport.getAppiumSettingsAppPackage());
    }

    protected static void addEmulatorToAvailableEmulatorsList(String emulatorName, String emulatorPort, ArrayList<String> apps, ArrayList<String> availableDevices) {
        if (!DriverSupport.isTestFinishedSuccessfully() && DriverSupport.isDeviceContainsApp(apps)) {
            MyLogger.log.info("Device: " + emulatorName + " has " + DriverSupport.getAppiumSettingsAppPackage() + " installed, and previous run did not finish successfully, uninstalling the app");
            uninstallEmulatorAppiumSettingsApp(emulatorPort);
            availableDevices.add(emulatorName);
        } else if (DriverSupport.isTestFinishedSuccessfully() && DriverSupport.isDeviceContainsApp(apps)) {
            MyLogger.log.info("Device: " + emulatorName + " has " + DriverSupport.getAppiumSettingsAppPackage() + " installed, assuming it is under testing");
        } else {
            availableDevices.add(emulatorName);
        }
    }

    static void setEmulatorPortId(String emulatorPortId) {
        EmulatorSupport.emulatorPortId = emulatorPortId;
    }

    static String getEmulatorPortId() {
        return emulatorPortId;
    }

    static ArrayList<String> removeNonEmulators(ArrayList<String> connectedEmulators) {
        connectedEmulators.removeIf(device -> !device.contains("emulator"));
        return connectedEmulators;
    }
}
