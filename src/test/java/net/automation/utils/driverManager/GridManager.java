package net.automation.utils.driverManager;

import net.automation.sut.utils.MyLogger;
import net.automation.utils.TestContextWrapper;
import org.openqa.grid.common.RegistrationRequest;
import org.openqa.grid.internal.utils.SelfRegisteringRemote;
import org.openqa.grid.internal.utils.configuration.GridHubConfiguration;
import org.openqa.grid.internal.utils.configuration.GridNodeConfiguration;
import org.openqa.grid.web.Hub;
import org.openqa.selenium.remote.server.SeleniumServer;
import org.testng.Reporter;

import java.net.*;
import java.util.Enumeration;

public class GridManager {
    private static GridNodeConfiguration gridNodeConfig;
    private static String nodePort;
    private static String ip;

    private static void setNodePort() {
        if (TestContextWrapper.getInstance().isDefaultSuite()) {
            nodePort = "5556";
        } else {
            nodePort = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("nodePort");
        }
    }

    public static void gridManager() {
        MyLogger.log.info("Configuring hub and node");
        ip = getLocalIPv4Address();

        HubConfig();
        NodeConfig();
    }

    public static void HubConfig() {
        GridHubConfiguration gridHubConfig = new GridHubConfiguration();
        gridHubConfig.host = ip;
        gridHubConfig.role = "hub";
        gridHubConfig.port = 4444;

        Hub myHub = new Hub(gridHubConfig);
        myHub.start();
    }

    public static void NodeConfig() {
        setNodePort();
        gridNodeConfig = new GridNodeConfiguration();
        gridNodeConfig.hub = "http://" + ip + ":4444/grid/register";
        gridNodeConfig.host = ip; //my ip address
        gridNodeConfig.port = Integer.parseInt(nodePort);
        gridNodeConfig.role = "node";

        RegistrationRequest req = RegistrationRequest.build(gridNodeConfig);
        SelfRegisteringRemote remote = new SelfRegisteringRemote(req);
        remote.setRemoteServer(new SeleniumServer(gridNodeConfig));
        remote.startRemoteServer();
        remote.startRegistrationProcess();
    }

    private static String getLocalIPv4Address(){
        String ip = null;
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                // filters out 127.0.0.1 and inactive interfaces
                if (iface.isLoopback() || !iface.isUp())
                    continue;

                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while(addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();

                    // *EDIT*
                    if (addr instanceof Inet6Address) continue;

                    ip = addr.getHostAddress();
                }
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        return ip;
    }

    public static GridNodeConfiguration getGridNodeConfig() {
        return gridNodeConfig;
    }
}