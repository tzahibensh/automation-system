package net.automation.utils.listeners;

import net.automation.tests.AbstractTest;
import net.automation.utils.jira.Jira;
import net.automation.utils.reports.ExtentTestReport;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryAnalyzer extends AbstractTest implements IRetryAnalyzer {

    public static RetryAnalyzer getInstance() {
        return RetryAnalyzer.InstanceHolder.INSTANCE;
    }

    static int retriesCounter = 1;
    int maxRetries = 3;

    @Override
    public boolean retry(ITestResult result) {
        if (retriesCounter < maxRetries) {
            retriesCounter++;
            report.removeTest(currentTestReport.getTest());
            return true;
        }
        if (System.getProperty("createJiraIssue").equals("true")) {
            Jira.getInstance().createJiraIssue(ExtentTestReport.resultMessage);
            currentTestReport.addInfo("Jira bug: " + "<a href=" + Jira.getInstance().getJiraBugUrl() + " target=\"_blank\">" + Jira.getInstance().getJiraBugNumber() + "</a>");
        }
        return false;
    }

    public boolean isRetriesCounterAchievedMaxRetries() {
        return Integer.toString(retriesCounter).equals(Integer.toString(maxRetries));
    }

    public int getIterationNumber() {
        return retriesCounter;
    }

    private static final class InstanceHolder {
        private static final RetryAnalyzer INSTANCE = new RetryAnalyzer();
    }
}