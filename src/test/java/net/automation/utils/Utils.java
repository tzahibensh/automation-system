package net.automation.utils;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utils {

    public static String getCurrentDateTime() {
        return new SimpleDateFormat("dd-MM-yyyy HH-mm-ss").format(Calendar.getInstance().getTime());
    }

    public static String capitalizeAndAddSpaces(String string) {
        string = StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(string), " ");
        return StringUtils.capitalize(string);
    }
}
