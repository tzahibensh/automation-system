package net.automation.utils;

import net.automation.sut.utils.MyLogger;
import org.apache.log4j.Level;
import org.testng.Reporter;

public class RuntimeSettingsManager {
    private static String baseUrl;

    public static RuntimeSettingsManager getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public String getPlatform() {
        return System.getProperty("platform");
    }

    public String getCellularDevice() {
        return System.getProperty("cellularDevice");
    }

    public static void setLoggerType() {
        switch (System.getProperty("loggerLevel")) {
            case "debug":
                MyLogger.log.setLevel(Level.DEBUG);
                break;
            case "info":
                MyLogger.log.setLevel(Level.INFO);
                break;
            case "log":
                MyLogger.log.setLevel(Level.ALL);
                break;
            case "warn":
                MyLogger.log.setLevel(Level.WARN);
                break;
            case "trace":
                MyLogger.log.setLevel(Level.TRACE);
                break;
            case "error":
                MyLogger.log.setLevel(Level.ERROR);
                break;
            default:
                MyLogger.log.setLevel(Level.OFF);
                break;
        }
    }

    public String getExecutionType() {
        return System.getProperty("executionType");
    }

    public static String getBaseUrl(){
        if(TestContextWrapper.getInstance().isDefaultSuite()){
            return System.getProperty("baseUrl");
        }else {
            return Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("baseUrl");
        }
    }

    private static final class InstanceHolder {
        private static final RuntimeSettingsManager INSTANCE = new RuntimeSettingsManager();
    }
}
