package net.automation.utils;

import net.automation.utils.reports.ExtentReport;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.testng.ITestContext;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.ArrayList;
import java.util.Collection;

public class ReportEmailing extends TestContext {
    private static final String recipient = "freshybs@gmail.com";
    private static final String from = "freshybs@gmail.com";
    private static final String password = "Wtcwtc1412";
    private static final String hostName = "smtp.gmail.com";
    private static final int smtpPort = 25;
    private static final boolean startTls = true;

    public static ReportEmailing getInstance() {
        return ReportEmailing.InstanceHolder.INSTANCE;
    }

    public void sendEmail(String reportName, ITestContext context) {
        if (System.getProperty("sendEmail").equals("true")) {
            emailManager(reportName, context);
        }
    }

    public void emailManager(String suiteName, ITestContext context) {
        try {
            MultiPartEmail email = new MultiPartEmail();
            email.setHostName(hostName);
            email.setSmtpPort(smtpPort);
            email.setStartTLSEnabled(startTls);
            email.setAuthenticator(new DefaultAuthenticator(from, password));
            email.setFrom(from);

            Collection<InternetAddress> collectionTo = new ArrayList<>();
            collectionTo.add(new InternetAddress(recipient));
            email.setTo(collectionTo);

            String[] testNamesArray = getFailedTestsNames(context);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(testNamesArray.length).append(" test(s) in ").append(suiteName).append(" suite failed:\n");
            email.setSubject(suiteName + " failed");

            int count = 0;
            for (String aTestNamesArray : testNamesArray) {
                String testName = aTestNamesArray;
                testName = testName.substring(0, testName.indexOf("."));
                /* + jiraIssues.get(count)*/
                stringBuilder.append(testName).append(": ");
            }
            stringBuilder.append("\nAttached: Report for ").append(suiteName).append(" suite tests, please download before opening.");

            email.setMsg(stringBuilder.toString());
            email.attach(createAttachment());
            email.send();
        } catch (MessagingException | EmailException e) {
            e.printStackTrace();
        }
    }

    private EmailAttachment createAttachment() {
        EmailAttachment attachment = new EmailAttachment();
        attachment.setPath(ExtentReport.getInstance().getReportPath());
        attachment.setDisposition(EmailAttachment.ATTACHMENT);
        return attachment;
    }

    private static final class InstanceHolder {
        private static final ReportEmailing INSTANCE = new ReportEmailing();
    }
}