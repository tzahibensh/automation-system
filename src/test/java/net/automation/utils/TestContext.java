package net.automation.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.TestResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestContext {
    public int numberOfFailedTests(ITestContext testContext) {
        return testContext.getFailedTests().size();
    }

    public int numberOfPassedTests(ITestContext testContext) {
        return testContext.getPassedTests().size();
    }

    public String[] getFailedTestsNames(ITestContext testContext) {
        String[] failedTestArray = new String[numberOfFailedTests(testContext)];
        int count = 0;

        for (ITestResult result : testContext.getFailedTests().getAllResults()) {
            String name = getName(result);

            if (!Arrays.asList(failedTestArray).contains(name)) {
                failedTestArray[count] = name;
            }
            count++;
        }

        int c = 0;
        for (String aFailedTestArray : failedTestArray) {
            if (aFailedTestArray != null) {
                c++;
            }
        }

        String[] failedTestArray2 = new String[c];

        System.arraycopy(failedTestArray, 0, failedTestArray2, 0, c);

        return failedTestArray2;
    }

    private static String getName(ITestResult result) {
        final List<String> parameters = new ArrayList<>();
        if (result.getParameters() != null) {
            for (Object parameter : result.getParameters()) {
                if (parameter instanceof TestResult && ((TestResult) parameter).getStatus() < 0) {
                    // TestResult.toString() will explode with status < 0, can't use the toString() method
                    parameters.add(parameter.getClass().getName() + "@" + parameter.hashCode());
                } else {
                    parameters.add(parameter == null ? "null" : parameter.toString());
                }
            }
        }

        return result.getTestClass().getRealClass().getSimpleName() + "." + result.getMethod().getMethodName() + "("
                + StringUtils.join(parameters, ",") + ")";
    }

    static int getId(ITestResult result) {
        final HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(result.getTestClass().getRealClass());
        builder.append(result.getMethod().getMethodName());
        builder.append(result.getParameters());
        return builder.toHashCode();
    }
}
