package net.automation.utils.jira;

public class Payload {
    String summary;
    //String description;
    IssueType issuetype;
    Projects project;

    public Payload(String summary, IssueType issueType, Projects project) {
        this.summary = summary;
        //this.description = description;
        this.issuetype = issueType;
        this.project = project;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public IssueType getIssuetype() {
        return issuetype;
    }

    public void setIssuetype(IssueType issuetype) {
        this.issuetype = issuetype;
    }

    public Projects getProject() {
        return project;
    }

    public void setProject(Projects project) {
        this.project = project;
    }
}