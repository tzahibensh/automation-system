package net.automation.utils.jira;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.automation.utils.TestContextWrapper;
import org.apache.http.HttpHeaders;

import java.util.Base64;

public class Jira {
    private static final String userEmail = "freshybs@gmail.com";
    private static final String userToken = "4CbpZgThIGA8LnZTQhM34930";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APP_JSON = "application/json";
    private static final String PROJECT = "AS";
    private static final String ISSUE_TYPE = "10004";
    private static final String projectUrl = "https://tzahi.atlassian.net/browse/";
    private String jiraBugNumber = "";
    public static String jiraBugUrl = "";

    public static Jira getInstance() {
        return Jira.InstanceHolder.INSTANCE;
    }

    public void createJiraIssue(String summary) {
        RequestSpecification request = RestAssured.given();

        request.header(HttpHeaders.AUTHORIZATION, "Basic " + Base64.getEncoder().encodeToString((userEmail + ":" + userToken).getBytes()));
        request.header(CONTENT_TYPE, APP_JSON);

        Projects project = new Projects(PROJECT);
        IssueType id = new IssueType(ISSUE_TYPE);
        Payload payload = new Payload(TestContextWrapper.getInstance().getTestName() + ": " + summary, id, project);
        Fields fields = new Fields(payload);

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(fields);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        request.body(fields);
        Response response = request.post("https://tzahi.atlassian.net/rest/api/3/issue");
        jiraBugNumber = response.jsonPath().getString("key");
        jiraBugUrl = projectUrl + jiraBugNumber;
    }

    public String getJiraBugUrl(){
        return jiraBugUrl;
    }

    public String getJiraBugNumber() {
        return jiraBugNumber;
    }

    private static final class InstanceHolder {
        private static final Jira INSTANCE = new Jira();
    }
}