package net.automation.utils.jira;

public class IssueType {
    String id;

    public IssueType(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String issueType) {
        this.id = issueType;
    }
}