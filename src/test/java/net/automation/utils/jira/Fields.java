package net.automation.utils.jira;

public class Fields {
    Payload fields;

    public Payload getFields() {
        return fields;
    }

    public void setFields(Payload payload) {
        this.fields = payload;
    }

    public Fields(Payload fields) {
        this.fields = fields;
    }
}
