package net.automation.tests.webPage.Settings.Config.Allocation;

import net.automation.sut.tests.webpage.NavigationBarWebPage;
import net.automation.tests.webPage.AbstractWebPageTest;
import net.automation.utils.listeners.RetryAnalyzer;
import org.testng.annotations.Test;

public class AllocationWebTest extends AbstractWebPageTest {

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void allocation() {
        NavigationBarWebPage.openSettingsMenu();
        NavigationBarWebPage.mouseOverConfig();
        NavigationBarWebPage.ClickOnAllocation();
    }
}