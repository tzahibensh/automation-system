package net.automation.tests.webPage;

import net.automation.sut.tests.webpage.Web;
import net.automation.tests.AbstractTest;
import net.automation.utils.RuntimeSettingsManager;
import net.automation.utils.ScreenShot;
import net.automation.utils.driverManager.DriverManager;
import net.automation.utils.driverManager.GridManager;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.IOException;
import java.lang.reflect.Method;

public class AbstractWebPageTest extends AbstractTest {

    @BeforeSuite
    @Override
    public void beforeSuite(ITestContext context) {
        super.beforeSuite(context);
        if(System.getProperty("platform").equals("desktopChrome")) {
            GridManager.gridManager();
        }
    }

    @BeforeClass
    @Override
    public void beforeClass(ITestContext context) {
        super.beforeClass(context);
    }

    @BeforeMethod
    @Override
    public void beforeMethod(ITestContext context, Method method) throws IOException {
        super.beforeMethod(context, method);
        DriverManager.getInstance().createDriver(RuntimeSettingsManager.getInstance().getPlatform());
    }

    @BeforeTest
    @Override
    public void beforeTest() {
    }

    @AfterTest
    @Override
    public void afterTest() {
    }

    @AfterMethod
    @Override
    public void afterMethod(ITestResult result, Method method) {
        ScreenShot.getInstance().screenShotManager(Web.remoteWebDriver.get(), reportName);
        super.afterMethod(result, method);
        DriverManager.getInstance().terminateDriver(RuntimeSettingsManager.getInstance().getPlatform());
    }

    @AfterSuite
    @Override
    public void afterSuite(ITestContext testContext) throws IOException {
        super.afterSuite(testContext);
        DriverManager.getInstance().terminateDriver(RuntimeSettingsManager.getInstance().getPlatform());
    }
}