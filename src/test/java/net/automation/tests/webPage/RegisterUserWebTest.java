package net.automation.tests.webPage;

import net.automation.sut.tests.webpage.BottomAlerts;
import net.automation.sut.tests.webpage.NavigationBarWebPage;
import net.automation.sut.tests.webpage.RegistrationWebPage;
import net.automation.utils.listeners.RetryAnalyzer;
import org.testng.annotations.Test;

public class RegisterUserWebTest extends AbstractWebPageTest {

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void registerAUser() {
        NavigationBarWebPage.clickOnOrganizationTab();
        currentTestReport.stepPass("Clicked on organization tab");
        NavigationBarWebPage.clickOnXyzItemInDropdown();
        currentTestReport.stepPass("Clicked on clickOnXyzItemInDropdown item in dropdown");
        NavigationBarWebPage.clickOnRegistrationTab();
        currentTestReport.stepPass("Clicked on clickOnXyzItemInDropdown item in dropdown");
        RegistrationWebPage.verifyFirstNameTitle();
        RegistrationWebPage.insertFirstName();
        currentTestReport.stepPass("Inserting first name automation");
        RegistrationWebPage.verifyLastNameTitle();
        RegistrationWebPage.insertLastName();
        currentTestReport.stepPass("Inserting last name automation");
        RegistrationWebPage.verifyIdTitle();
        RegistrationWebPage.insertIdNumber();
        currentTestReport.stepPass("Inserting ID number 123123");
        RegistrationWebPage.verifyAgeTitle();
        RegistrationWebPage.insertAge();
        currentTestReport.stepPass("Inserting age");
        RegistrationWebPage.verifyMobilePhoneTitle();
        RegistrationWebPage.insertMobilePhone();
        currentTestReport.stepPass("Inserting mobile phone 0555555555");
        RegistrationWebPage.verifyEmailTitle();
        RegistrationWebPage.insertEmail();
        currentTestReport.stepPass("Inserting email tzahi@cordio-med.com");
        RegistrationWebPage.verifyFemaleTitle();
        RegistrationWebPage.verifyMaleTitle();
        RegistrationWebPage.selectMaleGender();
        currentTestReport.stepPass("Selecting male gender");
        RegistrationWebPage.verifyMotherTongueTitle();
        currentTestReport.stepPass("Selecting male gender");
        RegistrationWebPage.insertMotherTonguePhone();
        currentTestReport.stepPass("Inserting HE mother tongue");
        RegistrationWebPage.verifyRegisterButtonTitle();
        RegistrationWebPage.clickOnRegisterButton();
        currentTestReport.stepPass("Clicking on the registration button");
        BottomAlerts.verifyBottomAlertsTextAndExtractUserAndPassword("The patient was successfully created! User Name");
        BottomAlerts.verifyBottomAlertsTextAndExtractUserAndPassword(", Password: ");
        currentTestReport.stepPass("Verifying successful registration message");
    }

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void registerAUserMinimalistic() {
        NavigationBarWebPage.clickOnOrganizationTab();
        currentTestReport.stepPass("Clicked on organization tab");
        NavigationBarWebPage.clickOnXyzItemInDropdown();
        currentTestReport.stepPass("Clicked on clickOnXyzItemInDropdown item in dropdown");
        NavigationBarWebPage.clickOnRegistrationTab();
        currentTestReport.stepPass("Clicked on clickOnXyzItemInDropdown item in dropdown");
        RegistrationWebPage.verifyFirstNameTitle();
        RegistrationWebPage.verifyLastNameTitle();
        RegistrationWebPage.verifyIdTitle();
        RegistrationWebPage.verifyAgeTitle();
        RegistrationWebPage.verifyMobilePhoneTitle();
        RegistrationWebPage.insertMobilePhone();
        currentTestReport.stepPass("Inserting mobile phone 0555555555");
        RegistrationWebPage.verifyEmailTitle();
        RegistrationWebPage.insertEmail();
        currentTestReport.stepPass("Inserting email tzahi@cordio-med.com");
        RegistrationWebPage.verifyFemaleTitle();
        RegistrationWebPage.verifyMaleTitle();
        RegistrationWebPage.verifyMotherTongueTitle();
        RegistrationWebPage.verifyRegisterButtonTitle();
        RegistrationWebPage.clickOnRegisterButton();
        currentTestReport.stepPass("Clicking on the registration button");
        BottomAlerts.verifyBottomAlertsTextAndExtractUserAndPassword("The patient was successfully created! User Name");
        BottomAlerts.verifyBottomAlertsTextAndExtractUserAndPassword(", Password: ");
        currentTestReport.stepPass("Verifying successful registration message");
    }

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void failRegisterAUserAllFieldsEmpty() {
        NavigationBarWebPage.clickOnOrganizationTab();
        currentTestReport.stepPass("Clicked on organization tab");
        NavigationBarWebPage.clickOnXyzItemInDropdown();
        currentTestReport.stepPass("Clicked on clickOnXyzItemInDropdown item in dropdown");
        NavigationBarWebPage.clickOnRegistrationTab();
        currentTestReport.stepPass("Clicked on clickOnXyzItemInDropdown item in dropdown");
        RegistrationWebPage.verifyFirstNameTitle();
        RegistrationWebPage.verifyLastNameTitle();
        RegistrationWebPage.verifyIdTitle();
        RegistrationWebPage.verifyAgeTitle();
        RegistrationWebPage.verifyMobilePhoneTitle();
        RegistrationWebPage.verifyEmailTitle();
        RegistrationWebPage.verifyFemaleTitle();
        RegistrationWebPage.verifyMaleTitle();
        RegistrationWebPage.verifyMotherTongueTitle();
        RegistrationWebPage.verifyRegisterButtonTitle();
        RegistrationWebPage.clickOnRegisterButton();
        currentTestReport.stepPass("Clicking on the registration button");
        BottomAlerts.verifyBottomAlertsTextAndExtractUserAndPassword("Email cannot be empty.");
        currentTestReport.stepPass("Verifying registration error message");
        RegistrationWebPage.errorMessage();
        currentTestReport.stepPass("Verifying registration error bottom message");
    }
}