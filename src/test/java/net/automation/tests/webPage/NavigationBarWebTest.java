package net.automation.tests.webPage;

import net.automation.sut.tests.webpage.NavigationBarWebPage;
import net.automation.utils.listeners.RetryAnalyzer;
import org.testng.annotations.Test;

public class NavigationBarWebTest extends AbstractWebPageTest {

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void menu() {
        NavigationBarWebPage.verifyLogo();
        currentTestReport.stepPass("Find the logo");
        NavigationBarWebPage.clickOnDashboardDropdown();
        currentTestReport.stepPass("Open tab");
    }
}