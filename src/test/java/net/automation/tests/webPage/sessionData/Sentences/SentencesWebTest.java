package net.automation.tests.webPage.sessionData.Sentences;

import net.automation.sut.tests.webpage.LoginWebPage;
import net.automation.sut.tests.webpage.SessionData.Sentences.EnSentencesWebPage;
import net.automation.sut.tests.webpage.BottomAlerts;
import net.automation.tests.webPage.AbstractWebPageTest;
import net.automation.utils.listeners.RetryAnalyzer;
import org.testng.annotations.Test;

public class SentencesWebTest extends AbstractWebPageTest {

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void createAnEnSentence() {
        LoginWebPage.login();
        currentTestReport.stepPass("User is logged in");
        EnSentencesWebPage.openSessionDataPage();
        EnSentencesWebPage.verifyPageTitle();
        EnSentencesWebPage.clickOnEnSentencesTab();
        EnSentencesWebPage.verifyTabTitles();
        EnSentencesWebPage.clickOnAddSentenceButton();
        EnSentencesWebPage.addSentenceFormTitle();
        EnSentencesWebPage.verifyIdentifierFieldTitle();
        EnSentencesWebPage.verifySentenceFieldTitle();
        EnSentencesWebPage.insertSentenceIdentifier();
        EnSentencesWebPage.insertSentence();
        EnSentencesWebPage.saveSentence();
        BottomAlerts.verifyBottomAlertsText("New sentence was successfully created");
        EnSentencesWebPage.verifySentenceAdded();
    }
}