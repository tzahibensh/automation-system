package net.automation.tests.webPage.sessionData;

import net.automation.sut.tests.webpage.LoginWebPage;
import net.automation.tests.webPage.AbstractWebPageTest;
import net.automation.utils.listeners.RetryAnalyzer;
import org.testng.annotations.Test;

public class SessionDataWebTest extends AbstractWebPageTest {

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void login() {
        LoginWebPage.insertUserName();
        currentTestReport.stepPass("Insert user name");

    }
}