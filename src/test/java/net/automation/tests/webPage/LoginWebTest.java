package net.automation.tests.webPage;

import net.automation.sut.tests.webpage.LoginWebPage;
import net.automation.tests.AbstractTest;
import net.automation.utils.listeners.RetryAnalyzer;
import org.testng.annotations.Test;

public class LoginWebTest extends AbstractWebPageTest {

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void login() {
        LoginWebPage.insertUserName();
        AbstractTest.currentTestReport.stepPass("Insert user name");
        LoginWebPage.insertPassword();
        AbstractTest.currentTestReport.stepPass("Insert password");
        LoginWebPage.clickOnLoginButton();
        AbstractTest.currentTestReport.stepPass("Click on login button");
        LoginWebPage.findNavigationBar();
        AbstractTest.currentTestReport.stepPass("Find the navigation bar");
    }
}