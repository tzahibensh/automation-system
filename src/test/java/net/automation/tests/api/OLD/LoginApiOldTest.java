package net.automation.tests.api.OLD;

import net.automation.sut.tests.api.headers.RequestHeaders;
import net.automation.sut.tests.api.OLD.LoginApiOld;
import net.automation.sut.tests.api.OLD.LoginBodyBuilderOld;
import net.automation.sut.utils.GoogleAPIs;
import net.automation.tests.AbstractTest;
import net.automation.tests.api.loginTests.LoginParameters;
import net.automation.utils.listeners.RetryAnalyzer;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class LoginApiOldTest extends AbstractTest {

    private static final String TEST_EMAIL_CORRECT = LoginParameters.getInstance().correctEmail();
    private static final String TEST_EMAIL_WRONG = "wrong@atomation.net";
    private static final String TEST_PASSWORD_CORRECT = "123456";
    private static final String TEST_PASSWORD_WRONG = "1";

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void loginCorrectCredentials() {
        LoginApiOld loginApiOld = new LoginApiOld();
        LoginBodyBuilderOld builder = new LoginBodyBuilderOld();
        builder.setEmail(TEST_EMAIL_CORRECT);
        builder.setPassword(GoogleAPIs.getDbRecords("B2"));
        LoginApiOld.LoginResponse response = loginApiOld.login(builder, RequestHeaders.contentType());
        assertEquals(response.getCode(), Integer.valueOf(200), "Wrong body status code: ");
        currentTestReport.stepPass("Body status code is: 200");
        assertEquals(response.getCode(), response.getStatusCode(), "Wrong status code: ");
        currentTestReport.stepPass("Status code is: 200");
        assertEquals(response.getMsg(), "OK", "Wrong response message: ");
        currentTestReport.stepPass("Message (msg) is: OK");
        assertNotNull(response.getToken(), "Token is Null");
        currentTestReport.stepPass("Token received");
    }

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void loginWrongPassword() {
        System.out.println("2");
        LoginApiOld loginApiOld = new LoginApiOld();
        LoginBodyBuilderOld builder = new LoginBodyBuilderOld();
        builder.setEmail(TEST_EMAIL_CORRECT);
        builder.setPassword(TEST_PASSWORD_WRONG);
        LoginApiOld.LoginResponse response = loginApiOld.login(builder, RequestHeaders.contentType());
        assertEquals(response.getCode(), Integer.valueOf(400), "Wrong status code: ");
        currentTestReport.stepPass("Status code is: 400");
        assertEquals(response.getMsg(), "Wrong password", "Wrong password response: ");
        currentTestReport.stepPass("Correct wrong-password response");
        assertNull(response.getToken(), "Token received: ");
        currentTestReport.stepPass("Token is null");
    }

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void loginWrongEmail() {
        LoginApiOld loginApiOld = new LoginApiOld();
        LoginBodyBuilderOld builder = new LoginBodyBuilderOld();
        builder.setEmail(TEST_EMAIL_WRONG);
        builder.setPassword(TEST_PASSWORD_CORRECT);
        LoginApiOld.LoginResponse response = loginApiOld.login(builder, RequestHeaders.contentType());
        assertEquals(response.getCode(), Integer.valueOf(404), "Wrong status code: ");
        currentTestReport.stepPass("Status code is: 404");
        assertEquals(response.getMsg(), "Wrong username", "Wrong username response: ");
        currentTestReport.stepPass("Correct wrong-username response");
        assertNull(response.getToken(), "Token received: ");
        currentTestReport.stepPass("Token is null");
    }

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void LoginIntPassword() {
        LoginApiOld loginApiOld = new LoginApiOld();
        LoginBodyBuilderOld builder = new LoginBodyBuilderOld();
        builder.setPassword(123456);
        builder.setEmail(TEST_EMAIL_CORRECT);
        LoginApiOld.LoginResponse response = loginApiOld.login(builder, RequestHeaders.contentType());
        //@TODO what are we expecting here?
        assertEquals(response.getCode(), Integer.valueOf(400), "Wrong status code: ");
        currentTestReport.stepPass("Status code is: 400");
        assertEquals(response.getMsg(), "Illegal arguments: number, string", "Wrong password response: ");
        currentTestReport.stepPass("Correct wrong-password response");
        assertNull(response.getToken(), "Token received: ");
        currentTestReport.stepPass("Token is null");
    }

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void loginNoEmail() {
        LoginApiOld loginApiOld = new LoginApiOld();
        LoginBodyBuilderOld builder = new LoginBodyBuilderOld(TEST_EMAIL_CORRECT, TEST_PASSWORD_CORRECT, false, true);
        LoginApiOld.LoginResponse response = loginApiOld.login(builder, RequestHeaders.contentType());
        assertEquals(response.getCode(), Integer.valueOf(406), "Wrong status code: ");
        currentTestReport.stepPass("Status code is: 406");
        assertEquals(response.getMsg(), "Wrong username", "Wrong username response: ");
        currentTestReport.stepPass("Correct wrong-username response");
        assertNull(response.getToken(), "Token received: ");
        currentTestReport.stepPass("Token is null");
    }

    //@TODO in postman, when sent no password, response was wrong email, what are we expecting?
}