package net.automation.tests.api.OLD;

import io.restassured.specification.RequestSpecification;
import net.automation.sut.tests.api.headers.RequestHeaders;
import net.automation.sut.tests.api.OLD.LoginApiOld;
import net.automation.sut.tests.api.OLD.LoginBodyBuilderOld;
import net.automation.tests.AbstractTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class AddDeviceTest extends AbstractTest {

    private static final String TEST_EMAIL_CORRECT = "vibration@atomation.net";
    private static final String TEST_PASSWORD_CORRECT = "123456";
    private final RequestSpecification request = RequestHeaders.authorization();

    @Test
    public void addDevice() {
        LoginApiOld loginApiOld = new LoginApiOld();
        LoginBodyBuilderOld builder = new LoginBodyBuilderOld();
        builder.setEmail(TEST_EMAIL_CORRECT);
        builder.setPassword(TEST_PASSWORD_CORRECT);
        LoginApiOld.LoginResponse response = loginApiOld.login(builder, request);
        assertEquals(response.getCode(), Integer.valueOf(200), "Wrong body status code: ");
        currentTestReport.stepPass("Body status code is: 200");
        assertEquals(response.getCode(), response.getStatusCode(), "Wrong status code: ");
        currentTestReport.stepPass("Status code is: 200");
        assertEquals(response.getMsg(), "OK", "Wrong response message: ");
        currentTestReport.stepPass("Message (msg) is: OK");
        String token = response.getToken();
        assertNotNull(token, "Token is Null");
        currentTestReport.stepPass("Token received");
    }
}