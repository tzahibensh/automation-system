/*
package net.automation.tests.api.OLD;

import io.restassured.specification.RequestSpecification;
import net.automation.sut.tests.api.headers.RequestHeaders;
import net.automation.sut.tests.api.addEvent.AddEventApi;
import net.automation.sut.tests.api.addEvent.AddEventBodyBuilder;
import net.automation.tests.AbstractTest;
import net.automation.utils.listeners.RetryAnalyzer;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class AddEventTest extends AbstractTest {
    private final RequestSpecification request = RequestHeaders.authorization();

    @Test(retryAnalyzer = RetryAnalyzer.class*/
/*, dependsOnMethods = "net.atomation.automation.tests.api.login.LoginApiTest.loginCorrectCredentials"*//*
)
    public void addEvent() {
        AddEventBodyBuilder builder = new AddEventBodyBuilder();
        builder.setEventState(11);
        builder.setValue(80);
        AddEventApi addEventApi = new AddEventApi();
        AddEventApi.addEventResponse response = addEventApi.addEvent(builder,request);

        assertEquals(response.getCode(), Integer.valueOf(200), "Wrong body status code: ");
        currentTestReport.stepPass("Body status code is: 200");
        assertEquals(response.getCode(), response.getStatusCode(), "Wrong status code: ");
        currentTestReport.stepPass("Status code is: 200");
        assertEquals(response.getMsg(), "OK", "Wrong response message: ");
        currentTestReport.stepPass("Message (msg) is: OK");
    }

    @Test(retryAnalyzer = RetryAnalyzer.class*/
/*, dependsOnMethods = "net.atomation.automation.tests.api.login.LoginApiTest.loginCorrectCredentials"*//*
)
    public void addEventNoAppToken() {
        AddEventBodyBuilder builder = new AddEventBodyBuilder();
        builder.setEventState(11);
        builder.setValue(80);
        builder.setAppToken(false);
        AddEventApi addEventApi = new AddEventApi();
        AddEventApi.addEventResponse response = addEventApi.addEvent(builder,request);

        assertEquals(response.getCode(), Integer.valueOf(503), "Wrong body status code: ");
        currentTestReport.stepPass("Body status code is: 503");
        assertEquals(response.getCode(), response.getStatusCode(), "Wrong status code: ");
        currentTestReport.stepPass("Status code is: 503");
        assertEquals(response.getMsg(), "ValidationError - DeviceEvent validation failed: app_token: Path `app_token` is required.", "Wrong response message: ");
        currentTestReport.stepPass("Message (msg) is: ValidationError - DeviceEvent validation failed: app_token: Path `app_token` is required.");
    }

    @Test(retryAnalyzer = RetryAnalyzer.class*/
/*, dependsOnMethods = "net.atomation.automation.tests.api.login.LoginApiTest.loginCorrectCredentials"*//*
)
    public void addEventNoMac() {
        AddEventBodyBuilder builder = new AddEventBodyBuilder();
        builder.setEventState(11);
        builder.setValue(80);
        builder.setMac(false);
        AddEventApi addEventApi = new AddEventApi();
        AddEventApi.addEventResponse response = addEventApi.addEvent(builder,request);

        assertEquals(response.getCode(), Integer.valueOf(503), "Wrong body status code: ");
        currentTestReport.stepPass("Body status code is: 503");
        assertEquals(response.getCode(), response.getStatusCode(), "Wrong status code: ");
        currentTestReport.stepPass("Status code is: 503");
        assertEquals(response.getMsg(), "ValidationError - DeviceEvent validation failed: mac: Path `mac` is required.", "Wrong response message: ");
        currentTestReport.stepPass("Message (msg) is: ValidationError - DeviceEvent validation failed: mac: Path `mac` is required.");
    }

    @Test(retryAnalyzer = RetryAnalyzer.class*/
/*, dependsOnMethods = "net.atomation.automation.tests.api.login.LoginApiTest.loginCorrectCredentials"*//*
)
    public void addEventNGwTimeGmt() {
        AddEventBodyBuilder builder = new AddEventBodyBuilder();
        builder.setEventState(11);
        builder.setValue(80);
        builder.setGwTimeGmt(false);
        AddEventApi addEventApi = new AddEventApi();
        AddEventApi.addEventResponse response = addEventApi.addEvent(builder,request);

        assertEquals(response.getCode(), Integer.valueOf(503), "Wrong body status code: ");
        currentTestReport.stepPass("Body status code is: 503");
        assertEquals(response.getCode(), response.getStatusCode(), "Wrong status code: ");
        currentTestReport.stepPass("Status code is: 503");
        assertEquals(response.getMsg(), "ValidationError - DeviceEvent validation failed: gw_time_gmt: Path `gw_time_gmt` is required.", "Wrong response message: ");
        currentTestReport.stepPass("Message (msg) is: ValidationError - DeviceEvent validation failed: gw_time_gmt: Path `gw_time_gmt` is required.");
    }
}*/
