package net.automation.tests.api.getSession;

import net.automation.sut.tests.api.getPatientInfo.GetPatientInfoApi;
import net.automation.sut.tests.api.getSession.GetSessionApi;
import net.automation.sut.tests.api.headers.RequestHeaders;
import net.automation.sut.tests.api.login.LoginApi;
import net.automation.sut.tests.api.login.LoginBodyBuilder;
import net.automation.sut.tests.api.utils.S3Actions;
import net.automation.sut.utils.GoogleAPIs;
import net.automation.sut.utils.MyLogger;
import net.automation.tests.AbstractTest;
import net.automation.tests.api.loginTests.LoginApiTest;
import net.automation.utils.listeners.RetryAnalyzer;
import org.testng.annotations.Test;

import static org.junit.Assert.assertThat;
import static org.junit.internal.matchers.StringContains.containsString;

public class GetSessionApiTest extends AbstractTest {

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void getSessionToVerifyFilesUploadedToS3() {
        currentTestReport.addInfo("Uploading file to S3");
        S3Actions.uploadAFileToS3(System.getProperty("user.dir") + "/s3/MKT-0730_200728_111111_S0018_he_1.68_iPhone7_iOS13.5.1_200728_111111_10300.wav",
                "cordio-app-test-origin");
        S3Actions.isFileExistInS3("cordio-app-test-origin", "/s3/MKT-0730_200728_111111_S0018_he_1.68_iPhone7_iOS13.5.1_200728_111111_10300.wav");

        currentTestReport.addInfo("Login api");
        MyLogger.log.info("Sending login request to get the token");
        LoginApi loginApi = new LoginApi();
        LoginBodyBuilder builder = new LoginBodyBuilder();
        builder.setUsername(LoginApiTest.USERNAME_CORRECT);
        builder.setPassword(LoginApiTest.PASSWORD_CORRECT);
        builder.setIpAddress(LoginApiTest.IPADDRESS_CORRECT);
        loginApi.login(builder, RequestHeaders.contentType(), "successful");

        currentTestReport.addInfo("GetPatientInfo API");
        MyLogger.log.info("Sending getPatientInfo request to get the latest session id");
        GetPatientInfoApi getPatientInfoApi = new GetPatientInfoApi();
        GetPatientInfoApi.GetPatientInfoResponse getPatientInfoResponse = getPatientInfoApi.getPatientInfo(RequestHeaders.authorization(), Integer.parseInt(GoogleAPIs.getDbRecords("Login!D2")));
        int latestSessionId = getPatientInfoResponse.getSessionId().get(0);
        currentTestReport.stepPass("Patient ID is as expected");

        currentTestReport.addInfo("GetSession API");
        MyLogger.log.info("Sending getSession request");
        GetSessionApi getSessionApi = new GetSessionApi();
        GetSessionApi.GetSessionResponse getSessionResponse = getSessionApi.getSession(RequestHeaders.authorization(), latestSessionId);
        MyLogger.log.info(getSessionResponse.getRecordsPathsList());
        assertThat(getSessionResponse.getRecordsPathsList().get(0), containsString("s3/MKT-0730_200728_111111_S0018_he_1.68_iPhone7_iOS13.5.1_200728_111111_10300.wav"));
    }
}