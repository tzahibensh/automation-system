package net.automation.tests.api;
import net.automation.sut.tests.api.utils.S3Actions;
import net.automation.utils.listeners.RetryAnalyzer;
import org.testng.annotations.Test;


public class UploadFiles {

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void uploadFiles() {
        S3Actions.uploadAFileToS3(System.getProperty("user.dir") + "s3/MKT-0730_200727_094340_S0018_he_1.68_iPhone7_iOS13.5.1_200727_094339_10300.wav",
                "cordio-app-test-origin");
        S3Actions.deleteFileFromS3();
    }
}
