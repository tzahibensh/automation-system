package net.automation.tests.api.getPatientInfoTests;

import net.automation.sut.tests.api.headers.RequestHeaders;
import net.automation.sut.tests.api.getPatientInfo.GetPatientInfoApi;
import net.automation.sut.tests.api.login.LoginApi;
import net.automation.sut.tests.api.login.LoginBodyBuilder;
import net.automation.sut.utils.GoogleAPIs;
import net.automation.sut.utils.MyLogger;
import net.automation.tests.AbstractTest;
import net.automation.tests.api.loginTests.LoginApiTest;
import net.automation.utils.listeners.RetryAnalyzer;
import org.testng.annotations.Test;

import java.util.Currency;

import static org.testng.Assert.*;

public class GetPatientInfoApiTest extends AbstractTest {

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void getPatientInfo() {
        currentTestReport.addInfo("Login with correct credentials");
        MyLogger.log.info("Sending login request to get the token");
        LoginApi loginApi = new LoginApi();
        LoginBodyBuilder builder = new LoginBodyBuilder();
        builder.setUsername(LoginApiTest.USERNAME_CORRECT);
        builder.setPassword(LoginApiTest.PASSWORD_CORRECT);
        builder.setIpAddress(LoginApiTest.IPADDRESS_CORRECT);
        loginApi.login(builder, RequestHeaders.contentType(), "successful");

        GetPatientInfoApi getPatientInfoApi = new GetPatientInfoApi();
        getPatientInfoApi.getPatientInfo(RequestHeaders.authorization(), Integer.parseInt(GoogleAPIs.getDbRecords("Login!D2")));
        currentTestReport.stepPass("Patient ID is as expected");
    }
}