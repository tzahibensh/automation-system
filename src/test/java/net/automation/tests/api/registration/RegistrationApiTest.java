package net.automation.tests.api.registration;

import net.automation.sut.tests.api.headers.RequestHeaders;
import net.automation.sut.tests.api.login.LoginApi;
import net.automation.sut.tests.api.login.LoginBodyBuilder;
import net.automation.sut.tests.api.registration.RegistrationApi;
import net.automation.sut.tests.api.registration.RegistrationBodyBuilder;
import net.automation.sut.utils.MyLogger;
import net.automation.tests.api.loginTests.LoginApiTest;
import net.automation.utils.listeners.RetryAnalyzer;
import net.automation.tests.AbstractTest;
import org.testng.annotations.Test;

public class RegistrationApiTest extends AbstractTest {

    public static final String EMAIL_CORRECT = "tzahi@cordio-med.com";
    public static final String CELLPHONE_CORRECT = "111222333444";
    public static final String ORGANIZATION_ID_CORRECT = "18";
    public static final String FIRSTNAME_CORRECT = "automation";
    public static final String LASTNAME_CORRECT = "automation";
    public static final String USERNAME_CORRECT = "automation";
    public static final String ID_NUMBER_CORRECT = "automation";
    public static final String START_DATE_CORRECT = "automation";
    public static final String LAST_STATUS_DATE_CORRECT = "automation";
    public static final String AGE_CORRECT = "automation";
    public static final String GENDER_CORRECT = "automation";
    public static final String LAST_HOSPITALIZATION_DATE_CORRECT = "automation";
    public static final String PATIENT_STATUS_TYPE_CORRECT = "automation";

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void registrationCorrectCredentialsMinimumParameters() {
        currentTestReport.addInfo("Login api");
        MyLogger.log.info("Sending login request to get the token");
        LoginApi loginApi = new LoginApi();
        LoginBodyBuilder loginBodyBuilder = new LoginBodyBuilder();
        loginBodyBuilder.setUsername(LoginApiTest.USERNAME_CORRECT);
        loginBodyBuilder.setPassword(LoginApiTest.PASSWORD_CORRECT);
        loginBodyBuilder.setIpAddress(LoginApiTest.IPADDRESS_CORRECT);
        loginApi.login(loginBodyBuilder, RequestHeaders.contentType(), "successful");
        currentTestReport.stepPass("Login api finished successfully");

        currentTestReport.addInfo("Register with minimum correct parameters");
        RegistrationApi registrationApi = new RegistrationApi();
        RegistrationBodyBuilder registrationBodyBuilder = new RegistrationBodyBuilder();
        registrationBodyBuilder.setEmail(EMAIL_CORRECT);
        registrationBodyBuilder.setCellphone(CELLPHONE_CORRECT);
        registrationBodyBuilder.setOrganizationId(ORGANIZATION_ID_CORRECT);
        RegistrationApi.RegistrationResponse response = registrationApi.registration(registrationBodyBuilder, RequestHeaders.registration());
        currentTestReport.stepPass("Status code is 200");
        currentTestReport.stepPass("Registration succeeded, new user created: " + response.getRegResponse());
    }

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void registrationCorrectCredentialsAllParameters() {
        currentTestReport.addInfo("Login api");
        MyLogger.log.info("Sending login request to get the token");
        LoginApi loginApi = new LoginApi();
        LoginBodyBuilder loginBodyBuilder = new LoginBodyBuilder();
        loginBodyBuilder.setUsername(LoginApiTest.USERNAME_CORRECT);
        loginBodyBuilder.setPassword(LoginApiTest.PASSWORD_CORRECT);
        loginBodyBuilder.setIpAddress(LoginApiTest.IPADDRESS_CORRECT);
        loginApi.login(loginBodyBuilder, RequestHeaders.contentType(), "successful");
        currentTestReport.stepPass("Login api finished successfully");

        currentTestReport.addInfo("Register with all correct parameters");
        RegistrationApi registrationApi = new RegistrationApi();
        RegistrationBodyBuilder registrationBodyBuilder = new RegistrationBodyBuilder();
        registrationBodyBuilder.setEmail(EMAIL_CORRECT);
        registrationBodyBuilder.setCellphone(CELLPHONE_CORRECT);
        registrationBodyBuilder.setOrganizationId(ORGANIZATION_ID_CORRECT);
        registrationBodyBuilder.setFirstName(FIRSTNAME_CORRECT);
        registrationBodyBuilder.setLastName(LASTNAME_CORRECT);
        registrationBodyBuilder.setUserName(USERNAME_CORRECT);
        registrationBodyBuilder.setIdNumber(ID_NUMBER_CORRECT);
        registrationBodyBuilder.setStartDate(START_DATE_CORRECT);
        registrationBodyBuilder.setLastStatusDate(LAST_STATUS_DATE_CORRECT);
        registrationBodyBuilder.setAge(AGE_CORRECT);
        registrationBodyBuilder.setGender(GENDER_CORRECT);
        registrationBodyBuilder.setLastHospitalizationDate(LAST_HOSPITALIZATION_DATE_CORRECT);
        registrationBodyBuilder.setPatientStatusType(PATIENT_STATUS_TYPE_CORRECT);
        RegistrationApi.RegistrationResponse response = registrationApi.registration(registrationBodyBuilder, RequestHeaders.registration());
        currentTestReport.stepPass("Status code is 200");
        currentTestReport.stepPass("Registration succeeded, new user created: " + response.getRegResponse());
    }
}
