package net.automation.tests.api.loginTests;

import net.automation.sut.utils.GoogleAPIs;

public class LoginParameters {
    public static LoginParameters getInstance() {
        return LoginParameters.InstanceHolder.INSTANCE;
    }

    public String correctEmail() {
        return GoogleAPIs.getDbRecords("Login!A2");
    }

    private static final class InstanceHolder {
        private static final LoginParameters INSTANCE = new LoginParameters();
    }
}
