package net.automation.tests.api.loginTests;

import net.automation.sut.tests.api.headers.RequestHeaders;
import net.automation.sut.tests.api.login.LoginApi;
import net.automation.sut.tests.api.login.LoginBodyBuilder;
import net.automation.tests.AbstractTest;
import net.automation.utils.listeners.RetryAnalyzer;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class LoginApiTest extends AbstractTest {

    public static final String USERNAME_CORRECT = "tzahi";
    private static final String USERNAME_WRONG = "asdasd";
    public static final String PASSWORD_CORRECT = "QWEqwe123!";
    private static final String PASSWORD_WRONG = "123456";
    public static final String IPADDRESS_CORRECT = "1.1.1.1";

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void loginCorrectCredentials() {
        currentTestReport.addInfo("Login with correct credentials");
        LoginApi loginApi = new LoginApi();
        LoginBodyBuilder builder = new LoginBodyBuilder();
        builder.setUsername(USERNAME_CORRECT);
        builder.setPassword(PASSWORD_CORRECT);
        builder.setIpAddress(IPADDRESS_CORRECT);
        LoginApi.LoginResponse response = loginApi.login(builder, RequestHeaders.contentType(), "successful");
        assertEquals(response.getStatusCode(), 200, "Wrong statusCode: ");
        currentTestReport.stepPass("Status code is 200");
        assertEquals(response.getUserName(), "tzahi", "Wrong username: ");
        currentTestReport.stepPass("User name: tzahi");
        assertNotNull(response.getAccessToken(), "AccessToken is Null");
        currentTestReport.stepPass("Access token received");
        assertNotNull(response.getRefreshToken(), "Refresh token is Null");
        currentTestReport.stepPass("Refresh token received");
        assertEquals(response.getUserStatus(), 0);
        currentTestReport.stepPass("UserStatus is: 0");
        assertFalse(response.getChangePasswordRequired(), "Wrong PasswordRequired: ");
        currentTestReport.stepPass("PasswordRequired: false");
    }

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void loginWrongPassword() {
        currentTestReport.addInfo("Login with wrong password");
        LoginApi loginApi = new LoginApi();
        LoginBodyBuilder builder = new LoginBodyBuilder();
        builder.setUsername(USERNAME_CORRECT);
        builder.setPassword(PASSWORD_WRONG);
        builder.setIpAddress(IPADDRESS_CORRECT);
        LoginApi.LoginResponse response = loginApi.login(builder, RequestHeaders.contentType(), "wrongPassword");
        assertEquals(response.getStatusCode(), 400, "Wrong statusCode: ");
        currentTestReport.stepPass("Status code is 400");
        assertEquals(response.getMessage(), "Username or password are incorrect.", "Wrong login error message");
        currentTestReport.stepPass("Correct login error message");
    }

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void loginWrongUsername() {
        currentTestReport.addInfo("Login with wrong username");
        LoginApi loginApi = new LoginApi();
        LoginBodyBuilder builder = new LoginBodyBuilder();
        builder.setUsername(USERNAME_WRONG);
        builder.setPassword(PASSWORD_CORRECT);
        builder.setIpAddress(IPADDRESS_CORRECT);
        LoginApi.LoginResponse response = loginApi.login(builder, RequestHeaders.contentType(), "wrongUsername");
        assertEquals(response.getStatusCode(), 400, "Wrong statusCode: ");
        currentTestReport.stepPass("Status code is 400");
        assertEquals(response.getMessage(), "Username or password are incorrect.", "Wrong login error message");
        currentTestReport.stepPass("Correct login error message");
    }

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void loginNoPassword() {
        currentTestReport.addInfo("Login with no password");
        LoginApi loginApi = new LoginApi();
        LoginBodyBuilder builder = new LoginBodyBuilder(null, USERNAME_CORRECT, IPADDRESS_CORRECT, true, false, true);
        LoginApi.LoginResponse response = loginApi.login(builder, RequestHeaders.contentType(), "noPassword");
        assertEquals(response.getStatusCode(), 400, "Wrong statusCode: ");
        currentTestReport.stepPass("Status code is 400");
        assertEquals(response.getMessage(), "Username or password cannot be empty.", "Wrong login error message");
        currentTestReport.stepPass("Correct login error message");
    }

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void loginNoUsername() {
        currentTestReport.addInfo("Login with no username");
        LoginApi loginApi = new LoginApi();
        LoginBodyBuilder builder = new LoginBodyBuilder(PASSWORD_CORRECT, null, IPADDRESS_CORRECT, false, true, true);
        LoginApi.LoginResponse response = loginApi.login(builder, RequestHeaders.contentType(), "noPassword");
        assertEquals(response.getStatusCode(), 400, "Wrong statusCode: ");
        currentTestReport.stepPass("Status code is 400");
        assertEquals(response.getMessage(), "Username or password cannot be empty.", "Wrong login error message");
        currentTestReport.stepPass("Correct login error message");
    }
}