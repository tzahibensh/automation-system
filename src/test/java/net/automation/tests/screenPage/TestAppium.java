package net.automation.tests.screenPage;

import net.automation.sut.tests.screen.api.android.Android;
import net.automation.utils.listeners.RetryAnalyzer;
import org.testng.annotations.Test;

public class TestAppium extends AbstractScreenTest {

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void appium() {
        //Android.app.speedTest.open();
        Android.app.speedTest.menu.tapSettings();
    }
}