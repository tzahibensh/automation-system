package net.automation.tests.screenPage;

import net.automation.tests.AbstractTest;
import net.automation.utils.RuntimeSettingsManager;
import net.automation.utils.ScreenShot;
import net.automation.utils.driverManager.DriverManager;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.IOException;
import java.lang.reflect.Method;

public class AbstractScreenTest extends AbstractTest {

    @BeforeSuite
    @Override
    public void beforeSuite(ITestContext context) {
        super.beforeSuite(context);
    }

    @BeforeClass
    @Override
    public void beforeClass(ITestContext context) {
        super.beforeClass(context);
    }

    @BeforeMethod
    @Override
    public void beforeMethod(ITestContext context, Method method) throws IOException {
        super.beforeMethod(context, method);
    }

    @BeforeTest
    @Override
    public void beforeTest() throws IOException {
        DriverManager.getInstance().createDriver(RuntimeSettingsManager.getInstance().getPlatform());
    }

    @AfterTest
    @Override
    public void afterTest() {
        DriverManager.getInstance().terminateDriver(RuntimeSettingsManager.getInstance().getPlatform());
    }

    @AfterMethod
    @Override
    public void afterMethod(ITestResult result, Method method) {
        ScreenShot.getInstance().screenShotManager(null, reportName);
        super.afterMethod(result, method);
    }

    @AfterSuite
    @Override
    public void afterSuite(ITestContext testContext) throws IOException {
        super.afterSuite(testContext);
    }
}