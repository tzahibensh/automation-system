package net.automation.tests;

import net.automation.utils.listeners.TestListener;
import net.automation.utils.ReportEmailing;
import net.automation.utils.TestContextWrapper;
import net.automation.utils.Utils;
import net.automation.utils.reports.ExtentReport;
import net.automation.utils.reports.IReport;
import net.automation.utils.reports.ITestGroupReport;
import net.automation.utils.reports.ITestReport;
import org.apache.commons.lang3.StringUtils;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.IOException;
import java.lang.reflect.Method;

import static net.automation.utils.RuntimeSettingsManager.setLoggerType;

@Listeners({TestListener.class})
public abstract class AbstractTest {

    public static IReport report;
    private ITestGroupReport testGroup;
    protected static ITestReport currentTestReport;

    protected String reportName;

    @BeforeSuite
    public void beforeSuite(ITestContext context) {
        setLoggerType();
        TestContextWrapper contextWrapper = new TestContextWrapper(context);
        reportName = contextWrapper.isDefaultSuite() ? contextWrapper.getTestGroupName() : contextWrapper.getSuiteName();
        reportName = StringUtils.capitalize(reportName + ", " + Utils.getCurrentDateTime());
        report = new ExtentReport(reportName);
    }

    @BeforeClass
    public void beforeClass(ITestContext context) {
        TestContextWrapper contextWrapper = new TestContextWrapper(context);
        String testGroupName = contextWrapper.getTestGroupName();
        testGroup = report.createNewTestGroup(testGroupName);
    }

    @BeforeMethod
    public void beforeMethod(ITestContext context, Method method) throws IOException {
        TestContextWrapper.getInstance().setTestName(method);
        currentTestReport = testGroup.addTest(TestContextWrapper.getInstance().getTestName());
    }

    @BeforeTest
    public void beforeTest() throws IOException {
    }

    @AfterMethod
    public void afterMethod(ITestResult result, Method method) {
        currentTestReport.conclude(result, currentTestReport);
    }

    @AfterClass
    public void afterClass() {
    }

    @AfterTest
    public void afterTest() {
    }

    @AfterSuite
    public void afterSuite(ITestContext context) throws IOException {
        report.generate();
        ReportEmailing.getInstance().sendEmail(reportName, context);
    }
}