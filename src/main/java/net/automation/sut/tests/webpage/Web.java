package net.automation.sut.tests.webpage;

import net.automation.sut.utils.ADB;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Web {
    public static WebDriver driver;
    public static ADB adb;
    public static ThreadLocal<RemoteWebDriver> remoteWebDriver = new ThreadLocal<>();
}
