package net.automation.sut.tests.webpage;

import org.openqa.selenium.WebElement;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class NavigationBarWebPage extends BaseWebPage {

    public static void verifyLogo() {
        findElements(".navbar-header img", "cssSelector").get(0).getAttribute("src");
    }

    public static void clickOnDashboardDropdown() {
        WebElement tabs = findElements("#navbarSupportedContent > ul.navbar-nav", "cssSelector").get(0);
        List<WebElement> dashboard = findElementChild(tabs, "li a", "cssSelector");
        assertEquals(getText(dashboard.get(0)), "DASHBOARD", "Verifying the first tab is DASHBOARD");
        click(dashboard.get(0));
    }

    public static void goToSessionDataPage() {
        WebElement tabs = findElements("#navbarSupportedContent > ul.navbar-nav", "cssSelector").get(0);
        List<WebElement> settings = findElementChild(tabs, "li a", "cssSelector");
        assertEquals(getText(settings.get(8)), "SETTINGS", "Verifying the first tab is Settings");
        click(settings.get(8));
        List<WebElement> sad = findElementChild(settings.get(0), "li a .dropdown-item", "cssSelector");
        assertEquals(getText(settings.get(0)), "CONFIG", "Verifying the first tab is Settings");
        click(settings.get(0));
    }

    public static void clickOnDashboardInDashboardDropdown() {
        WebElement tabs = findElements("//a[@href='/dashboard-page']", Selectors.xPath).get(0);
        assertEquals(getText(tabs), "DASHBOARD", "Verifying the second item is DASHBOARD");
        click(tabs);
    }

    public static void clickOnOrganizationTab() {
        click(findElements("menu-organizations", Selectors.id).get(0));
    }

    public static void clickOnXyzItemInDropdown() {
        click(findElements("//a[contains(text(), 'XYZMedicalTestAutomaionFramework')]", Selectors.xPath).get(0));
    }

    public static void clickOnRegistrationTab() {
        WebElement registration = findElements("//a[@href='/registration']", Selectors.xPath).get(0);
        assertEquals(getText(registration), "REGISTRATION", "Verifying the second item text is REGISTRATION");
        click(registration);
    }

    public static void openSettingsMenu() {
        WebElement tabs = findElements("#navbarSupportedContent > ul.navbar-nav li a", "cssSelector").get(8);
        assertEquals(getText(tabs), "SETTINGS", "Verifying the first tab is Settings");
        click(tabs);
    }

    public static void mouseOverConfig(){
        //"$(li a:contains('CONFIG')")
        //WebElement webElement = jsExecutor("$('li a:contains('CONFIG')').get(0)");
        WebElement webElement = findElement("triangle", Selectors.className);
        mouseOver(webElement);
    }

    public static void ClickOnAllocation(){
        WebElement webElement = findElement("//a[@href='/config-allocation']", Selectors.xPath);
        click(webElement);
    }
}