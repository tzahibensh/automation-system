package net.automation.sut.tests.webpage;

import org.openqa.selenium.WebElement;

import static org.testng.Assert.assertEquals;

public class RegistrationWebPage extends BaseWebPage {

    public static void verifyFirstNameTitle() {
        WebElement webElement = findElements("#FirstName .label-left", Selectors.cssSelector).get(0);
        assertEquals(getText(webElement), "First Name", "First name title verification");
    }

    public static void insertFirstName(){
        WebElement webElement = findElements(".form-field-input#FirstName", Selectors.cssSelector).get(0);
        click(webElement);
        sendKeys(webElement, "automation");
    }

    public static void verifyLastNameTitle() {
        WebElement webElement = findElements("#LastName .label-left", Selectors.cssSelector).get(0);
        assertEquals(getText(webElement), "Last Name", "First name title verification");
    }

    public static void insertLastName(){
        WebElement webElement = findElements("#LastName.form-field-input", Selectors.cssSelector).get(0);
        click(webElement);
        sendKeys(webElement, "automation");
    }

    public static void verifyIdTitle() {
        WebElement webElement = findElements("#IDNumber .label-left", Selectors.cssSelector).get(0);
        assertEquals(getText(webElement), "ID Number", "ID Number title verification");
    }

    public static void insertIdNumber(){
        WebElement webElement = findElement("#IDNumber #IDNumber", Selectors.cssSelector);
        click(webElement);
        sendKeys(webElement, "123123");
    }

    public static void verifyAgeTitle(){
        WebElement webElement = findElement("#Age .label-left", Selectors.cssSelector);
        assertEquals(getText(webElement), "Age", "Age title verification");

    }

    public static void insertAge(){
        WebElement webElement = findElement("#Age #Age", Selectors.cssSelector);
        click(webElement);
        sendKeys(webElement, "33");
    }

    public static void verifyMobilePhoneTitle(){
        WebElement webElement = findElement("#MobilePhone .label-left", Selectors.cssSelector);
        assertEquals(getText(webElement), "Mobile Phone", "Mobile Phone title verification");

    }

    public static void insertMobilePhone(){
        WebElement webElement = findElement("#MobilePhone #MobilePhone", Selectors.cssSelector);
        click(webElement);
        sendKeys(webElement, "0555555555");
    }

    public static void verifyEmailTitle(){
        WebElement webElement = findElement("#Email .label-left", Selectors.cssSelector);
        assertEquals(getText(webElement), "Email", "Email title verification");
    }

    public static void insertEmail(){
        WebElement webElement = findElement("#Email #Email", Selectors.cssSelector);
        click(webElement);
        sendKeys(webElement, "tzahi@cordio.med.com");
    }

    public static void verifyFemaleTitle(){
        WebElement webElement = findElement("#mat-radio-2 .mat-radio-label-content", Selectors.cssSelector);
        assertEquals(getText(webElement), "Female", "Female title verification");
    }

    public static void verifyMaleTitle(){
        WebElement webElement = findElement("#mat-radio-3 .mat-radio-label-content", Selectors.cssSelector);
        assertEquals(getText(webElement), "Male", "Male title verification");
    }

    public static void selectMaleGender(){
        WebElement webElement = findElement("#mat-radio-3 .mat-radio-container", Selectors.cssSelector);
        click(webElement);
    }

    public static void verifyMotherTongueTitle(){
        WebElement webElement = findElement("#MotherTounge .label-left", Selectors.cssSelector);
        assertEquals(getText(webElement), "Mother Tounge", "Mother Tounge title verification");
    }

    public static void insertMotherTonguePhone(){
        WebElement webElement = findElement("#MotherTounge #MotherTounge", Selectors.cssSelector);
        click(webElement);
        sendKeys(webElement, "HE");
    }

    public static void verifyRegisterButtonTitle(){
        WebElement webElement = findElement(".submit-button-container .button", Selectors.cssSelector);
        assertEquals(getText(webElement), "Register & Request Password", "MotherTongue title verification");
    }

    public static void clickOnRegisterButton(){
        WebElement webElement = findElement(".submit-button-container .button", Selectors.cssSelector);
        click(webElement);
    }

    public static void errorMessage(){
        WebElement webElement = findElement("server-errors", Selectors.className);
        assertEquals(getText(webElement), "Email cannot be empty.", "Empty email title verification");
    }
}