package net.automation.sut.tests.webpage;

public final class Selectors {
    public static final String cssSelector = "cssSelector";
    public static final String className = "className";
    public static final String id = "id";
    public static final String xPath = "xPath";
    public static final String tagName = "tagName";

}
