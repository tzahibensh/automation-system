package net.automation.sut.tests.webpage.SessionData;

import net.automation.sut.tests.webpage.BaseWebPage;
import org.testng.Reporter;

public class SessionDataWebPage extends BaseWebPage {
    private static final String userName = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("userName");
    private static final String password = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("password");

    public static void insertUserName() {
        elementName = "UserName field";
        sendKeys(findElements(".form-field-input#UserName", "cssSelector").get(0), userName);
    }


}