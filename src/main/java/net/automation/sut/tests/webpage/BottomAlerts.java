package net.automation.sut.tests.webpage;

import net.automation.sut.utils.GoogleAPIs;
import org.openqa.selenium.WebElement;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class BottomAlerts extends BaseWebPage {

    public static void verifyBottomAlertsText(String textToVerify) {
        WebElement webElement = waitForVisibilityOfElement(".trb-message", Selectors.cssSelector);
        String messageText = getText(webElement);
        assertTrue(messageText.contains(textToVerify), "Expected: '" + textToVerify + "' but found: '" + messageText + "'");
    }

    public static void extractUserAndPassword(String text) {
        GoogleAPIs.writeToCell("Patients!A2", text.substring(49, 57));
        GoogleAPIs.writeToCell("Patients!B2", text.substring(69, 77));
    }

    public static void verifyBottomAlertsTextAndExtractUserAndPassword(String textToVerify) {
        WebElement webElement = waitForVisibilityOfElement(".trb-message", Selectors.cssSelector);
        String messageText = getText(webElement);
        assertTrue(messageText.contains(textToVerify));
        extractUserAndPassword(messageText);
    }
}