package net.automation.sut.tests.webpage.SessionData.Sentences;

import net.automation.sut.tests.webpage.BaseWebPage;
import net.automation.sut.utils.MyLogger;
import net.automation.sut.tests.webpage.Selectors;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class EnSentencesWebPage extends BaseWebPage {

    public static void openSessionDataPage() {
        navigateToUrl("https://web.dev.cordiomed.com/session-data");
    }

    public static void verifyPageTitle() {
        WebElement webElement = findElement(".container.col-lg-10 h3", Selectors.cssSelector);
        assertEquals(getText(webElement), "Session Data:");
    }

    public static void clickOnEnSentencesTab() {
        MyLogger.log.debug("Clicking on the En sentence tab");
        WebElement webElement = waitForVisibilityOfElement("//a[@href='#menu2']", Selectors.xPath);
        assertEquals(getText(webElement), "EN Sentences");
        click(webElement);
    }

    public static void verifyTabTitles() {
        WebElement identifier = waitForVisibilityOfElement("#menu2 .table-head-1", Selectors.cssSelector);
        assertEquals(getText(identifier), "Identifier");
        WebElement sentence = findElement("#menu2 .table-head-2", Selectors.cssSelector);
        assertEquals(getText(sentence), "Sentence");
    }

    public static void clickOnAddSentenceButton() {
        WebElement webElement = findElement("#menu2 .btn-primary", Selectors.cssSelector);
        scrollToCenter(webElement);
        click(webElement);
    }

    public static void addSentenceFormTitle() {
        WebElement webElement = waitForVisibilityOfElement("#sentences-modal .modal-header h5", Selectors.cssSelector);
        assertEquals(getText(webElement), "Add/Edit Sentence");
    }

    public static void verifyIdentifierFieldTitle() {
        WebElement webElement = findElements("#sentences-modal .form-group label", Selectors.cssSelector).get(0);
        assertEquals(getText(webElement), "Identifier:");
    }

    public static void verifySentenceFieldTitle() {
        WebElement webElement = findElements("#sentences-modal .form-group label", Selectors.cssSelector).get(1);
        assertEquals(getText(webElement), "Sentence:");
    }

    public static void insertSentenceIdentifier() {
        WebElement webElement = findElements(".modal-content .form-control", Selectors.cssSelector).get(0);
        sendKeys(webElement, "S-Automation");
    }

    public static void insertSentence() {
        WebElement webElement = findElements(".modal-content .form-control", Selectors.cssSelector).get(1);
        sendKeys(webElement, "S-Automation");
    }

    public static void saveSentence() {
        WebElement webElement = findElements(".modal-content .btn-success", Selectors.cssSelector).get(0);
        click(webElement);
    }

    public static void verifySentenceAdded() {
        List<WebElement> sentencesList = findElements("#menu2 tr", Selectors.cssSelector);
        boolean isSentenceExist = false;
        for (WebElement list : sentencesList) {
            WebElement webElement = findElementChild(list, "td", Selectors.cssSelector).get(0);
            String a = getText(webElement);
            if (getText(webElement).equals("S-Automation")) {
                isSentenceExist = true;
                break;
            }
        }
        assertTrue(isSentenceExist, "Could not find sentence");
    }
}