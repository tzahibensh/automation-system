package net.automation.sut.tests.webpage;

import org.testng.Reporter;

public class LoginWebPage extends BaseWebPage {
    private static final String userName = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("userName");
    private static final String password = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("password");

    public static void insertUserName() {
        elementName = "UserName field";
        sendKeys(findElements(".form-field-input#UserName", Selectors.cssSelector).get(0), userName);
    }

    public static void insertPassword() {
        elementName = "UserName field";
        sendKeys(findElements(".form-field-input#Password", Selectors.cssSelector).get(0), password);
    }

    public static void clickOnLoginButton(){
        elementName = "Login button";
        click(findElements(".button.primary.flat", Selectors.cssSelector).get(0));
    }

    public static void findNavigationBar(){
        //Use this method to verify login was successful
        elementName = "Navigatoin bar";
        findElements("navbarSupportedContent", Selectors.id).get(0);
    }

    public static void login(){
        insertUserName();
        insertPassword();
        clickOnLoginButton();
        findNavigationBar();
    }
}