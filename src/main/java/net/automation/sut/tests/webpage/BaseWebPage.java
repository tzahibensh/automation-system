package net.automation.sut.tests.webpage;

import net.automation.sut.utils.MyLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.testng.Assert.*;
import static org.testng.Assert.assertEquals;

public class BaseWebPage {

    protected static String elementName;

    public static void navigateToUrl(String url) {
        boolean navigated = true;
        try {
            Web.remoteWebDriver.get().get(url);
        } catch (WebDriverException e) {
            navigated = false;
        }
        assertTrue(navigated, "Unable to navigate to: " + url);
    }

    protected static WebElement findElement(String element, String elementType) {
        WebElement webElement;
        switch (elementType) {
            case "className":
                webElement = Web.remoteWebDriver.get().findElement(By.className(element));
                break;
            case "id":
                webElement = Web.remoteWebDriver.get().findElement(By.id(element));
                break;
            case "tagName":
                webElement = Web.remoteWebDriver.get().findElement(By.tagName(element));
                break;
            case "xPath":
                webElement = Web.remoteWebDriver.get().findElement(By.xpath(element));
                break;
            default:
                webElement = Web.remoteWebDriver.get().findElement(By.cssSelector(element));
                break;
        }

        return webElement;
    }

    protected static List<WebElement> findElements(String element, String elementType) {
        List<WebElement> elements;
        switch (elementType) {
            case "className":
                elements = Web.remoteWebDriver.get().findElements(By.className(element));
                break;
            case "id":
                elements = Web.remoteWebDriver.get().findElements(By.id(element));
                break;
            case "tagName":
                elements = Web.remoteWebDriver.get().findElements(By.tagName(element));
                break;
            case "xPath":
                elements = Web.remoteWebDriver.get().findElements(By.xpath(element));
                break;
            default:
                elements = Web.remoteWebDriver.get().findElements(By.cssSelector(element));
                break;
        }

        assertNotNull(elements, "Could not find element " + element);

        return elements;
    }

    protected static List<WebElement> findElementChild(WebElement webElement, String selector, String elementType) {
        List<WebElement> elements;
        switch (elementType) {
            case Selectors.className:
                elements = webElement.findElements(By.className(selector));
                break;
            case Selectors.id:
                elements = webElement.findElements(By.id(selector));
                break;
            case Selectors.tagName:
                elements = webElement.findElements(By.tagName(selector));
                break;
            default:
                elements = webElement.findElements(By.cssSelector(selector));
                break;
        }

        return elements;
    }

    protected static void click(WebElement element) {
        element.click();
    }

    protected static void sendKeys(WebElement element, String keys) {
        boolean sent = true;
        try {
            element.sendKeys(keys);
        } catch (WebDriverException e) {
            sent = false;
        }
        assertTrue(sent, "Could not send keys to: " + elementName + ", " + element);
    }

    protected static String getText(WebElement webElement) {
        return webElement.getText();
    }

    protected static WebElement waitForVisibilityOfElement(String elementName, String selectors) {
        MyLogger.log.debug("Wait for visibility of element");
        switch (selectors){
            case Selectors.cssSelector:
                return (new WebDriverWait(Web.remoteWebDriver.get(), 10)).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(elementName)));
            case Selectors.id:
                return (new WebDriverWait(Web.remoteWebDriver.get(), 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id(elementName)));
            default:
                return (new WebDriverWait(Web.remoteWebDriver.get(), 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementName)));
        }
    }

    protected static void scrollToCenter(WebElement webElement) {
        String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";

        ((JavascriptExecutor) Web.remoteWebDriver.get()).executeScript(scrollElementIntoMiddle, webElement);
    }

    protected static void mouseOver(WebElement webElement) {
        new Actions(Web.remoteWebDriver.get()).moveToElement(webElement).build().perform();
    }

    protected static WebElement jsExecutor(String elementName) {
        JavascriptExecutor js = Web.remoteWebDriver.get();
        return (WebElement) js.executeScript(elementName);
    }
}
