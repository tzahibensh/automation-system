package net.automation.sut.tests.screen.api.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import net.automation.sut.utils.ADB;
import net.automation.sut.tests.screen.api.apps.Apps;

public class Android {
    public static AndroidDriver<MobileElement> driver;
    public static ADB adb;
    public static Apps app = new Apps();
}
