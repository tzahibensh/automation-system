package net.automation.sut.tests.screen.api.apps.speedTest.menu;

import net.automation.sut.tests.screen.core.UiObject;
import net.automation.sut.tests.screen.core.UiSelector;

public class MenuUiObjects {
    private static UiObject speed, results, settings;

    public UiObject speed(){
        if(speed == null){
            speed = new UiSelector().text("Speed").makeUiObject();
        }
        return speed;
    }

    public UiObject results(){
        if(results == null){
            results = new UiSelector().text("Results").makeUiObject();
        }
        return results;
    }

    public UiObject settings(){
        if(settings == null){
            settings = new UiSelector().text("Settings").makeUiObject();
        }
        return settings;
    }
}
