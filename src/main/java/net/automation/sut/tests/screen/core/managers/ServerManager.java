package net.automation.sut.tests.screen.core.managers;

import java.io.IOException;
import java.util.Scanner;

public class ServerManager {
    private static String Os;
    private static String ANDROID_HOME;

    public static String getAndroidHome() {
        if (ANDROID_HOME == null) {
            ANDROID_HOME = System.getenv("ANDROID_HOME");

        }
        if (ANDROID_HOME == null) {
            throw new RuntimeException("Failed to find ANDROID_HOME, make sure the environment variable is set");
        }
        return ANDROID_HOME;
    }

    public static String getOs() {
        if (Os == null) {
            Os = System.getenv("os.name");
        }
        return Os;
    }

    public static boolean isWindows() {
        return getOs().startsWith("windows");
    }

    public static boolean isMac() {
        return getOs().startsWith("mac");
    }
}
