package net.automation.sut.tests.screen.api.apps.androidHome;

import net.automation.sut.utils.MyLogger;

import java.util.NoSuchElementException;

public class AndroidOS {
    public AndroidOSUiObjects UiObjects = new AndroidOSUiObjects();

    public boolean isAppIconVisible() {
        try {
            MyLogger.log.info("Tapping on the 'Speed test' app to open it");
            if(UiObjects.speedTest().waitToAppear(1)){
                return true;
            }
        } catch (NoSuchElementException e) {
            throw new AssertionError("Cant tap on 'Speed test' app, element does not exist or blocked");
        }
        return false;
    }

    public void openApp() {
        try {
            MyLogger.log.info("Tapping on the 'Speed test' app to open it");
            UiObjects.speedTest().waitToAppearMandatory(30).tap();
        } catch (NoSuchElementException e) {
            throw new AssertionError("Cant tap on 'Speed test' app, element does not exist or blocked");
        }
    }

    public boolean isNoRecentItemsExist() {
        try {
            MyLogger.log.info("Tapping on the 'Speed test' app to open it");
            if (UiObjects.noRecentItems().waitToAppear(2)) {
                return true;
            }
        } catch (NoSuchElementException e) {
            throw new AssertionError("Cant tap on 'Speed test' app, element does not exist or blocked");
        }
        return false;
    }
}
