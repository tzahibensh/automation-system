package net.automation.sut.tests.screen.api.apps.speedTest.menu;

import net.automation.sut.utils.MyLogger;

import java.util.NoSuchElementException;

public class Menu {
    public MenuUiObjects uiObjects = new MenuUiObjects();

    public void tapSpeed() {
        try {
            MyLogger.log.info("Tapping on the 'Speed' button");
            uiObjects.speed().waitToAppearMandatory(10).tap();
        } catch (NoSuchElementException e) {
            throw new AssertionError("Cant tap on 'Speed' button, element does not exist or blocked");
        }
    }

    public void tapResults() {
        try {
            MyLogger.log.info("Tapping on the 'Results' button");
            uiObjects.results().waitToAppearMandatory(10).tap();
        } catch (NoSuchElementException e) {
            throw new AssertionError("Cant tap on 'Results' button, element does not exist or blocked");
        }
    }

    public void tapSettings() {
        try {
            MyLogger.log.info("Tapping on the 'Settings' button");
            uiObjects.settings().waitToAppearMandatory(30).tap();
        } catch (NoSuchElementException e) {
            throw new AssertionError("Cant tap on 'Settings' button, element does not exist or blocked");
        }
    }
}
