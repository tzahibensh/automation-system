package net.automation.sut.tests.screen.core;

import net.automation.sut.utils.MyLogger;
import net.automation.sut.tests.screen.api.android.Android;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;

public class UiObject {
    private final String locator;

    UiObject(String locator) {
        this.locator = locator;
        MyLogger.log.debug("Created UI object: " + this.locator);
    }

    private boolean isXpath() {
        return !locator.contains("UiSelector");
    }

    public boolean exists() {
        try {
            WebElement element;
            if (isXpath()) {
                element = Android.driver.findElementByXPath(locator);
            } else {
                element = Android.driver.findElementByAndroidUIAutomator(locator);
            }
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isChecked() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        return element.getAttribute("checked").equals("true");
    }

    public boolean isCheckable() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        return element.getAttribute("checkable").equals("true");
    }

    public boolean isClickable() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        boolean a = element.getAttribute("clickable").equals("true");
        return a;
    }

    public boolean isEnabled() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        return element.getAttribute("enabled").equals("true");
    }

    public boolean isFocusable() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        return element.getAttribute("focusable").equals("true");
    }

    public boolean isFocused() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        return element.getAttribute("focused").equals("true");
    }

    public boolean isScrollable() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        return element.getAttribute("scrollable").equals("true");
    }

    public boolean isLongClickable() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        return element.getAttribute("longClikcable").equals("true");
    }

    public boolean isSelected() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        return element.getAttribute("selected").equals("true");
    }

    public Point getLocation() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        return element.getLocation();
    }

    public String getText() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        return element.getAttribute("name");
    }

    public String getResourceId() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        return element.getAttribute("resourceId");
    }

    public String getClassName() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        return element.getAttribute("className");
    }

    public String getContentDesc() {
        WebElement element;
        if (isXpath()) {
            element = Android.driver.findElementByXPath(locator);
        } else {
            element = Android.driver.findElementByAndroidUIAutomator(locator);
        }
        return element.getAttribute("contentDesc");
    }

    public UiObject clearText() {
        if (isXpath()) {
            Android.driver.findElementByXPath(locator).clear();
        } else {
            Android.driver.findElementByAndroidUIAutomator(locator).clear();
        }
        return this;
    }

    public UiObject typeText(String value) {
        if (isXpath()) {
            Android.driver.findElementByXPath(locator).sendKeys();
        } else {
            Android.driver.findElementByAndroidUIAutomator(locator).sendKeys();
        }
        return this;
    }

    public UiObject tap() {
        if (isXpath()) {
            Android.driver.findElementByXPath(locator).click();
        } else {
            Android.driver.findElementByAndroidUIAutomator(locator).click();
        }
        return this;
    }

    public UiObject scrollTo() {
        TouchActions action = new TouchActions(Android.driver);
        if (isXpath()) {
            action.scroll(Android.driver.findElementByXPath(locator), 10, 100).perform();
            //Android.driver.findElementByAndroidUIAutomator("locator.substring(locator.indexOf(\"@text=\\\"\"), locator.indexOf(\"\\\"]\")).replace(\"@text=\\\"\", \"\"))");
        } else {
            action.scroll(Android.driver.findElementByAndroidUIAutomator(locator), 10, 100);
        }
        return this;
    }

    public boolean waitToAppear(int seconds) {
        Timer timer = new Timer();
        timer.start();
        while (!timer.expired(seconds)) {
            if (exists()) {
                return true;
            }
        }
        return false;
    }

    public UiObject waitToAppearMandatory(int seconds) {
        Timer timer = new Timer();
        timer.start();
        while (!timer.expired(seconds)) {
            if (exists()) {
                break;
            }
            if (timer.expired(seconds) && !exists()) {
                throw new AssertionError("Element " + locator + " did not appear within " + seconds + " seconds");
            }
        }
        return this;
    }

    public UiObject waitToDisappear(int seconds) {
        Timer timer = new Timer();
        timer.start();
        while (!timer.expired(seconds)) {
            if (!exists()) {
                break;
            }
            if (timer.expired(seconds) && exists()) {
                throw new AssertionError("Element " + locator + " did not disappear within " + seconds + " seconds");
            }
        }
        return this;
    }
}
