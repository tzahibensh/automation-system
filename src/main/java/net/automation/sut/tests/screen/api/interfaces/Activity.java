package net.automation.sut.tests.screen.api.interfaces;

public interface Activity {
    Object waitToLoad();
}
