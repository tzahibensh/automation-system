package net.automation.sut.tests.screen.api.apps.androidHome;

import net.automation.sut.tests.screen.core.UiObject;
import net.automation.sut.tests.screen.core.UiSelector;

public class AndroidOSUiObjects {
    private static UiObject speedTest, noRecentItems;

    public UiObject speedTest() {
        if (speedTest == null) {
            speedTest = new UiSelector().description("Speedtest").makeUiObject();
        }
        return speedTest;
    }

    public UiObject noRecentItems() {
        if (noRecentItems == null) {
            noRecentItems = new UiSelector().text("No recent items").makeUiObject();
        }
        return noRecentItems;
    }
}
