package net.automation.sut.tests.screen.api.apps;

import net.automation.sut.tests.screen.api.apps.androidHome.AndroidOS;
import net.automation.sut.tests.screen.api.apps.speedTest.SpeedTest;

public class Apps {
    public SpeedTest speedTest = new SpeedTest();
    public AndroidOS androidOS = new AndroidOS();
}
