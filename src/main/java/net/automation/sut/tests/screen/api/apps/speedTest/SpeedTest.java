package net.automation.sut.tests.screen.api.apps.speedTest;

import net.automation.sut.tests.screen.api.android.Android;
import net.automation.sut.tests.screen.api.apps.speedTest.menu.Menu;
import net.automation.sut.tests.screen.api.interfaces.Application;

public class SpeedTest implements Application {
    public Menu menu = new Menu();

    @Override
    public void forceStop() {
        Android.adb.forceAppStop(packageId());
    }

    @Override
    public void clearData() {
        Android.adb.clearAppData(packageId());
    }

    @Override
    public Object open() {
        Android.app.androidOS.openApp();
        return null;
    }

    @Override
    public String packageId() {
        return "org.zwanoo.android.speedtest";
    }

    @Override
    public String activityId() {
        return "com.ookla.mobile4.screens.main.MainViewActivity";
    }
}
