package net.automation.sut.tests.api.registration;

import org.json.simple.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class RegistrationBodyBuilder {
    @Nullable
    private Object email = null;

    @Nullable
    private Object cellphone = null;

    @Nullable
    private Object organizationId = null;

    @Nullable
    private Object firstName = null;

    @Nullable
    private Object lastName = null;

    @Nullable
    private Object userName = null;

    @Nullable
    private Object idNumber = null;

    @Nullable
    private Object startDate = null;

    @Nullable
    private Object lastStatusDate = null;

    @Nullable
    private Object age = null;

    @Nullable
    private Object gender = null;

    @Nullable
    private Object lastHospitalizationDate = null;

    @Nullable
    private Object patientStatusType = null;

    public void setEmail(@Nonnull Object email) {
        this.email = email;
    }

    public void setCellphone(@Nullable Object cellphone) {
        this.cellphone = cellphone;
    }

    public void setOrganizationId(@Nullable Object organizationId) {
        this.organizationId = organizationId;
    }

    public void setFirstName(@Nullable Object firstName) {
        this.firstName = firstName;
    }

    public void setLastName(@Nullable Object lastName) {
        this.lastName = lastName;
    }

    public void setUserName(@Nullable Object userName) {
        this.userName = userName;
    }

    public void setIdNumber(@Nullable Object idNumber) {
        this.idNumber = idNumber;
    }

    public void setStartDate(@Nullable Object startDate) {
        this.startDate = startDate;
    }

    public void setLastStatusDate(@Nullable Object lastStatusDate) {
        this.lastStatusDate = lastStatusDate;
    }

    public void setAge(@Nullable Object age) {
        this.age = age;
    }

    public void setGender(@Nullable Object gender) {
        this.gender = gender;
    }

    public void setLastHospitalizationDate(@Nullable Object lastHospitalizationDate) {
        this.lastHospitalizationDate = lastHospitalizationDate;
    }

    public void setPatientStatusType(@Nullable Object patientStatusType) {
        this.patientStatusType = patientStatusType;
    }

    @Nonnull
    public Map<String, Object> registrationJSN() {
        Map<String,Object> json = new HashMap<>();
        if (email != null) {
            json.put("Email", email);
        }
        if (cellphone != null) {
            json.put("Cellphone", cellphone);
        }
        if (organizationId != null) {
            json.put("Organizationid", organizationId);
        }
        if (firstName != null) {
            json.put("FirstName", firstName);
        }
        if (lastName != null) {
            json.put("LastName", lastName);
        }
        if (userName != null) {
            json.put("Username", userName);
        }
        if (idNumber != null) {
            json.put("IDNumber", idNumber);
        }
        if (startDate != null) {
            json.put("StartDate", startDate);
        }
        if (lastStatusDate != null) {
            json.put("LastStatusDate", lastStatusDate);
        }
        if (age != null) {
            json.put("Age", age);
        }
        if (gender != null) {
            json.put("Gender", gender);
        }
        if (lastHospitalizationDate != null) {
            json.put("LastHospitalizationDate", lastHospitalizationDate);
        }
        if (patientStatusType != null) {
            json.put("PatientStatusType", patientStatusType);
        }
        return new JSONObject(json);
    }
}
