package net.automation.sut.tests.api.getSession;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.automation.sut.tests.api.getPatientInfo.GetPatientInfoApi;
import net.automation.sut.utils.GoogleAPIs;
import net.automation.sut.utils.MyLogger;

import java.util.List;

import static org.testng.Assert.assertNotNull;

public class GetSessionApi {
    private static final String GET_SESSION_ENDPOINT = "https://app.dev.cordiomed.com/api/Session/GetSession?sessionId=";

    public GetSessionResponse getSession(RequestSpecification request, int sessionId) {
        MyLogger.log.info("Sending getSession Get request");
        Response response = request.get(GET_SESSION_ENDPOINT + sessionId);

        MyLogger.log.debug(response.getBody().asString());

        int statusCode = response.getStatusCode();
        int id = response.jsonPath().getInt("Id");
        List<String> recordsPathsList = response.getBody().jsonPath().getList("Records.Path");

        return new GetSessionResponse(
                statusCode,
                id,
                recordsPathsList
        );
    }

    public static class GetSessionResponse {
        final int statusCode;
        final int id;
        final List<String> recordsPathsList;

        public GetSessionResponse(int statusCode, int id, List<String> recordsPathsList) {
            this.statusCode = statusCode;
            this.id = id;
            this.recordsPathsList = recordsPathsList;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public int getId() {
            return id;
        }

        public List<String> getRecordsPathsList() {
            return recordsPathsList;
        }
    }
}