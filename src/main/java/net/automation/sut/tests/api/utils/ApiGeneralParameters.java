package net.automation.sut.tests.api.utils;

import net.automation.sut.utils.GoogleAPIs;

public class ApiGeneralParameters {
    public static ApiGeneralParameters getInstance() {
        return ApiGeneralParameters.InstanceHolder.INSTANCE;
    }

    public String getAccessTypeKey() {
        return GoogleAPIs.getDbRecords("API!A1");
    }

    public String getAccessTypeValue() {
        return GoogleAPIs.getDbRecords("API!B1");
    }

    public String getContentTypeKey() {
        return GoogleAPIs.getDbRecords("API!A2");
    }

    public String getContentTypeValue() {
        return GoogleAPIs.getDbRecords("API!B2");
    }

    public String getAppVersionKey() {
        return GoogleAPIs.getDbRecords("API!A3");
    }

    public String getAppVersionValue() {
        return GoogleAPIs.getDbRecords("API!B3");
    }

    private static final class InstanceHolder {
        private static final ApiGeneralParameters INSTANCE = new ApiGeneralParameters();
    }
}
