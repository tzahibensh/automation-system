package net.automation.sut.tests.api.login;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.automation.sut.utils.GoogleAPIs;
import net.automation.sut.utils.MyLogger;

import javax.annotation.Nullable;

import static org.testng.Assert.assertEquals;

public class LoginApi {
    private static final String LOGIN_ENDPOINT = "https://app.dev.cordiomed.com/api/auth/SignIn";

    public LoginResponse login(LoginBodyBuilder builder, RequestSpecification request, String testCase) {
        MyLogger.log.info("Creating a login response object");
        request.body(builder.loginJSN().toString());
        Response response = request.post(LOGIN_ENDPOINT);

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Wrong statusCode: ");
        MyLogger.log.info("Login API succeeded with status code 200");

        String UserName = "";
        String RefreshToken = "";
        String AccessToken = "";
        int UserStatus = 0;
        boolean ChangePasswordRequired = false;
        String message = "";

        if ("successful".equals(testCase)) {
            UserStatus = response.jsonPath().getInt("UserStatus");
            UserName = response.jsonPath().getString("UserName");
            RefreshToken = response.jsonPath().getString("RefreshToken");
            AccessToken = response.jsonPath().getString("AccessToken");
            ChangePasswordRequired = response.jsonPath().getBoolean("ChangePasswordRequired");
        } else {
            message = response.jsonPath().getString("Message");
        }

        GoogleAPIs.writeToCell("Login!C2", AccessToken);

        return new LoginResponse(
                statusCode,
                AccessToken,
                RefreshToken,
                UserName,
                UserStatus,
                ChangePasswordRequired,
                message
        );
    }

    public static class LoginResponse {
        final int statusCode;

        @Nullable
        final String AccessToken;

        @Nullable
        final String RefreshToken;

        @Nullable
        final String UserName;

        final int UserStatus;

        final boolean ChangePasswordRequired;

        @Nullable
        final String message;

        public LoginResponse(int statusCode, @Nullable String AccessToken, @Nullable String RefreshToken, @Nullable String UserName, int UserStatus, boolean ChangePasswordRequired, @Nullable String message) {
            this.statusCode = statusCode;
            this.AccessToken = AccessToken;
            this.RefreshToken = RefreshToken;
            this.UserName = UserName;
            this.UserStatus = UserStatus;
            this.ChangePasswordRequired = ChangePasswordRequired;
            this.message = message;
        }

        public int getStatusCode() {
            return statusCode;
        }

        @Nullable
        public String getAccessToken() {
            return AccessToken;
        }

        @Nullable
        public String getRefreshToken() {
            return RefreshToken;
        }

        @Nullable
        public String getUserName() {
            return UserName;
        }

        public int getUserStatus() {
            return UserStatus;
        }

        public boolean getChangePasswordRequired() {
            return ChangePasswordRequired;
        }

        @Nullable
        public String getMessage() {
            return message;
        }
    }
}