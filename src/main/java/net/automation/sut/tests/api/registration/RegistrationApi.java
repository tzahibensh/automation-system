package net.automation.sut.tests.api.registration;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.automation.sut.utils.GoogleAPIs;
import net.automation.sut.utils.MyLogger;

import javax.annotation.Nullable;

import static org.testng.Assert.assertEquals;

public class RegistrationApi {

    private static final String REGISTRATION_ENDPOINT = "https://app.dev.cordiomed.com/API/PatientManagement/Add";

    public RegistrationResponse registration(RegistrationBodyBuilder builder, RequestSpecification request) {
        MyLogger.log.info("Sending Registration request and creating a response object");
        request.body(builder.registrationJSN().toString());
        Response response = request.post(REGISTRATION_ENDPOINT);

        MyLogger.log.info(response.getBody().asString());

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Wrong statusCode: ");
        MyLogger.log.info("Registration API succeeded with status code 200");

        String regResponse = response.getBody().asString();
        String username = regResponse.substring(2, 10);
        String password = regResponse.substring(13, 21);

        GoogleAPIs.writeToCell("Registration!A2", username);
        GoogleAPIs.writeToCell("Registration!B2", password);

        return new RegistrationResponse(
                regResponse
        );
    }

    public static class RegistrationResponse {

        final String regResponse;

        public RegistrationResponse(String regResponse) {
            this.regResponse = regResponse;
        }

        @Nullable
        public String getRegResponse() {
            return regResponse;
        }
    }
}
