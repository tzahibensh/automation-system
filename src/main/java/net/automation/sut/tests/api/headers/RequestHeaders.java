package net.automation.sut.tests.api.headers;


import io.restassured.specification.RequestSpecification;
import net.automation.sut.utils.GoogleAPIs;

import static io.restassured.RestAssured.given;

public class RequestHeaders {

    private static final String CONTENT_TYPE_KEY = "Content-Type";
    private static final String CONTENT_TYPE_VAL_JSON = "application/json";
    private static final String AUTHORIZATION = "Authorization";

    public static RequestSpecification contentType() {
        RequestSpecification request = given();
        request.header(CONTENT_TYPE_KEY, CONTENT_TYPE_VAL_JSON);

        return request;
    }

    public static RequestSpecification authorization() {
        RequestSpecification request = given();
        request.header(AUTHORIZATION, GoogleAPIs.getDbRecords("Login!C2"));

        return request;
    }

    public static RequestSpecification registration() {
        RequestSpecification request = given();
        request.header(CONTENT_TYPE_KEY, CONTENT_TYPE_VAL_JSON);
        request.header(AUTHORIZATION, GoogleAPIs.getDbRecords("Login!C2"));

        return request;
    }
}