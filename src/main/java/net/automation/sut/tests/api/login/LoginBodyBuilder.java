package net.automation.sut.tests.api.login;

import org.json.simple.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class LoginBodyBuilder {
    @Nullable
    private Object Username = null;

    @Nullable
    private Object password = null;

    @Nullable
    private Object IpAddress = null;

    private boolean sendUsername = true;
    private boolean sendPass = true;
    private boolean sendIpAddress = true;

    public LoginBodyBuilder(@Nullable Object password, @Nullable Object Username, Object IpAddress,
                            boolean sendUsername, boolean sendPass, boolean sendIpAddress) {
        this.Username = Username;
        this.password = password;
        this.IpAddress = IpAddress;
        this.sendIpAddress = sendIpAddress;
        this.sendUsername = sendUsername;
        this.sendPass = sendPass;
    }

    public LoginBodyBuilder() {
    }

    public void setPassword(@Nullable Object password) {
        this.password = password;
    }

    public void setUsername(@Nonnull Object Username) {
        this.Username = Username;
    }

    public void setIpAddress(@Nonnull Object IpAddress) {
        this.IpAddress = IpAddress;
    }

    @Nonnull
    public JSONObject loginJSN() {
        JSONObject json = new JSONObject();
        if (sendUsername) {
            json.put("Username", Username);
        }
        if (sendPass) {
            json.put("password", password);
        }
        if (sendIpAddress) {
            json.put("IpAddress", IpAddress);
        }
        return json;
    }
}