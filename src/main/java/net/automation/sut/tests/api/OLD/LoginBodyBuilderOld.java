package net.automation.sut.tests.api.OLD;

import org.json.simple.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class LoginBodyBuilderOld {
    @Nullable
    private Object email = null;

    @Nullable
    private Object password = null;

    private boolean sendEmail = true;
    private boolean sendPass = true;

    public LoginBodyBuilderOld(@Nullable Object password, @Nullable Object email, boolean sendEmail, boolean sendPass) {
        this.email = email;
        this.password = password;
        this.sendEmail = sendEmail;
        this.sendPass = sendPass;
    }

    public LoginBodyBuilderOld() {
    }

    public void setPassword(@Nullable Object password) {
        this.password = password;
    }

    public void setEmail(@Nonnull Object email) {
        this.email = email;
    }

    @Nonnull
    public JSONObject loginJSN() {
        JSONObject json = new JSONObject();
        if (sendEmail) {
            json.put("email", email);
        }
        if (sendPass) {
            json.put("password", password);
        }
        return json;
    }
}