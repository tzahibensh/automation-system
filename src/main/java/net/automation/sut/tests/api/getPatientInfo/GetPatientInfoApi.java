package net.automation.sut.tests.api.getPatientInfo;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.automation.sut.utils.GoogleAPIs;
import net.automation.sut.utils.MyLogger;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class GetPatientInfoApi {
    private static final String GET_PATIENT_INFO_ENDPOINT = "https://app.dev.cordiomed.com/api/Patient/GetPatientInformation?patientId=486";

    public GetPatientInfoResponse getPatientInfo(RequestSpecification request, int expectedPatientId) {
        MyLogger.log.info("Sending login Post request");
        Response response = request.get(GET_PATIENT_INFO_ENDPOINT);

        MyLogger.log.debug(response.getBody().asString());

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Wrong statusCode: ");
        MyLogger.log.info("getPatientInfo request succeeded with status code 200");

        int patientId = response.jsonPath().getInt("Id");
        assertEquals(patientId, expectedPatientId /*MKT-0730*/);

        List<Integer> sessionIdList = response.getBody().jsonPath().getList("Sessions.Id");

        GoogleAPIs.writeToCell("Login!D2", String.valueOf(patientId));
        assertNotNull(sessionIdList, "SessionId is null or empty");

        return new GetPatientInfoResponse(
                statusCode,
                patientId,
                sessionIdList
        );
    }

    public static class GetPatientInfoResponse {
        final int statusCode;
        final int patientId;
        final List<Integer> sessionIdList;

        public GetPatientInfoResponse(int statusCode, int patientId, List<Integer> sessionIdList) {
            this.statusCode = statusCode;
            this.patientId = patientId;
            this.sessionIdList = sessionIdList;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public int getId() {
            return patientId;
        }

        public List<Integer> getSessionId() {
            return sessionIdList;
        }
    }
}