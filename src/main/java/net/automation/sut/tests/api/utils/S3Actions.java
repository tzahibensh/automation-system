package net.automation.sut.tests.api.utils;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import net.automation.sut.utils.MyLogger;

import java.io.File;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class S3Actions {

    private static final String ACCESS_KEY = "AKIAUBHM4OKFX3KBY7NJ";
    private static final String SECRET_KEY = "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";
    private static final String ORIGIN_BUCKET = "cordio-app-test-origin";

    public static String getORIGIN_BUCKET() {
        return ORIGIN_BUCKET;
    }

    private static AmazonS3 amazonS3;

    public static void s3Init() {
        BasicAWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
        amazonS3 = AmazonS3Client.builder()
                .withRegion(Regions.EU_WEST_1)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();

    }

    public static void uploadAFileToS3(String filePath, String bucketName) {
        MyLogger.log.debug("Uploading file to S3");

        s3Init();

        //to verify the process had succeeded
        boolean requestStatus = false;

        try {
            amazonS3.putObject(bucketName, filePath, new File(filePath));
            requestStatus = true;
        } catch (AmazonServiceException ase) {
            MyLogger.log.info("Amazon service exception, request rejected with an error response");
            MyLogger.log.info(ase.getErrorMessage());
            MyLogger.log.info(ase.getStatusCode());
            MyLogger.log.info(ase.getErrorCode());
            MyLogger.log.info(ase.getErrorType());
            MyLogger.log.info(ase.getRequestId());
        } catch (AmazonClientException ace) {
            MyLogger.log.info("Amazon client exception, client encountered an internal error while trying to communicate with S3 (maybe unable to access the network)");
            MyLogger.log.info(ace.getMessage());
        }
        assertTrue(requestStatus, "Could not upload file");
    }

    public static void deleteFileFromS3() {
        MyLogger.log.debug("Deleting file from S3");

        s3Init();
        String bucketName = "cordio-app-test-origin";
        String keyName = "sadasdsa";
        boolean requestStatus = false;

        try {
            amazonS3.deleteObject(bucketName, keyName);
            requestStatus = true;
        } catch (AmazonServiceException ase) {
            MyLogger.log.info("Amazon service exception, request rejected with an error response");
            MyLogger.log.info(ase.getErrorMessage());
            MyLogger.log.info(ase.getStatusCode());
            MyLogger.log.info(ase.getErrorCode());
            MyLogger.log.info(ase.getErrorType());
            MyLogger.log.info(ase.getRequestId());
        } catch (AmazonClientException ace) {
            MyLogger.log.info("Amazon client exception, client encountered an internal error while trying to communicate with S3 (maybe unable to access the network");
            MyLogger.log.info(ace.getMessage());
        }
        assertTrue(requestStatus, "Could not delete file");
    }

    public static boolean isFileExistInS3(String bucketName, String fileName) {
        s3Init();

        try {
            ObjectListing objectListing = amazonS3.listObjects(bucketName);
            List<S3ObjectSummary> objectSummaries = objectListing.getObjectSummaries();
            for (S3ObjectSummary objectSummary : objectSummaries) {
                if (objectSummary.getKey().equals(fileName)) {
                    return true;
                }
            }
        } catch (AmazonServiceException ase) {
            MyLogger.log.info("Amazon service exception, request rejected with an error response");
            MyLogger.log.info(ase.getErrorMessage());
            MyLogger.log.info(ase.getStatusCode());
            MyLogger.log.info(ase.getErrorCode());
            MyLogger.log.info(ase.getErrorType());
            MyLogger.log.info(ase.getRequestId());
        }
        return false;
    }
}