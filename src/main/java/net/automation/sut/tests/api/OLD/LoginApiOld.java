package net.automation.sut.tests.api.OLD;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.automation.sut.tests.api.OLD.LoginBodyBuilderOld;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class LoginApiOld {
    private static final String LOGIN_ENDPOINT = "https://test.atomation.net/api/v1/auth/login";

    public LoginResponse login(LoginBodyBuilderOld builder, RequestSpecification request) {

        request.body(builder.loginJSN().toString());
        Response response = request.post(LOGIN_ENDPOINT);

        String msg = response.jsonPath().getString("msg");
        Integer codeBody = response.jsonPath().getInt("code");
        int statusCode = response.getStatusCode();
        String token = response.jsonPath().getString("data.token");

        return new LoginResponse(
                token,
                codeBody,
                msg,
                statusCode
        );
    }

    public static class LoginResponse {
        @Nullable
        final String token;

        @Nullable
        final Integer code;

        final int statusCode;

        @Nonnull
        final String msg;

        public LoginResponse(@Nullable String token, @Nullable Integer code, @Nonnull String msg, int statusCode) {
            this.token = token;
            this.code = code;
            this.msg = msg;
            this.statusCode = statusCode;
        }

        @Nullable
        public String getToken() {
            return token;
        }

        @Nullable
        public Integer getCode() {
            return code;
        }

        @Nullable
        public Integer getStatusCode() {
            return statusCode;
        }

        @Nonnull
        public String getMsg() {
            return msg;
        }
    }
}