package net.automation.sut.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ClearValuesRequest;
import com.google.api.services.sheets.v4.model.ClearValuesResponse;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;

import static org.testng.Assert.assertNotNull;

public class GoogleAPIs {
    private static final String APPLICATION_NAME = "Automation";
    private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.dir"), "sheets.googleapis.com-java-quickstart");
    private static FileDataStoreFactory DATA_STORE_FACTORY;
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static HttpTransport HTTP_TRANSPORT;
    private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS);
    private static final String spreadsheetId = "1gagnlW5IPbysfRZyEYhqW5X_iVcxObbYBZx6-9uAA6c";

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    public static Credential authorize() {
        Credential credential = null;
        // Load client secrets.
        try {
            FileInputStream in = new FileInputStream("client_secret.json");
            GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

            // Build flow and trigger user authorization request.
            GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                    .setDataStoreFactory(DATA_STORE_FACTORY)
                    .setAccessType("offline")
                    .build();
            credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
        } catch (IOException e) {
            MyLogger.log.debug(e.getMessage());
        }
        return credential;
    }

    public static Sheets getSheetsService() {
        Credential credential = authorize();
        return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    public static String getDbRecords(String range) {
        String record = "";
        try {
            Sheets service = getSheetsService();
            Sheets.Spreadsheets.Values.Get request = service.spreadsheets().values().get(spreadsheetId, range);
            ValueRange response;
            response = request.execute();

            List<List<Object>> values = response.getValues();
            //assertNotNull(values, "Google sheet record is null, ");

            if(values == null){
                MyLogger.log.debug("DB record is empty");
                return "";
            }

            for (List row : values) {
                //assertNotNull(row.get(0), "Google sheet record is null, ");
                MyLogger.log.debug(row.get(0));
                record = row.get(0).toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return record;
    }

    public static void deleteCell(String range) {
        ClearValuesRequest requestBody = new ClearValuesRequest();
        Sheets service = getSheetsService();
        try {
            Sheets.Spreadsheets.Values.Clear request = service.spreadsheets().values().clear(spreadsheetId, range, requestBody);
            ClearValuesResponse response = request.execute();
            MyLogger.log.debug(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeToCell(String cell, String value) {
        Sheets sheetsService = getSheetsService();
        try {
            List<Object> data1 = new ArrayList<>();
            data1.add(value);
            List<List<Object>> data = new ArrayList<>();
            data.add(data1);
            ValueRange oRange = new ValueRange();
            oRange.setRange(cell);
            oRange.setValues(data);
            Sheets.Spreadsheets.Values.Update request;
            request = sheetsService.spreadsheets().values().update(spreadsheetId, cell, oRange);
            request.setValueInputOption("RAW");
            UpdateValuesResponse response = request.execute();
            MyLogger.log.debug(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> getValuesOfCellsInRange(String range) {
        List<List<Object>> values;
        ArrayList<String> cellsInRange = new ArrayList<>();
        try {
            Sheets service = getSheetsService();
            Sheets.Spreadsheets.Values.Get request = service.spreadsheets().values().get(spreadsheetId, range);
            ValueRange response;
            response = request.execute();

            values = response.getValues();

            for (List row : values) {
                for (Object o : row) {
                    cellsInRange.add(o.toString());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cellsInRange;
    }
}