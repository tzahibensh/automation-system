package net.automation.sut.utils;

import java.util.ArrayList;

public class GeneralCommands {

    public static String killTask(String portId) {
        return RunCommand.runCommand("taskkill /F /PID " + portId);
    }

    public static ArrayList<String> getConnectedEmulatorDevices() {
        ArrayList<String> devices = RunCommand.runCommand2("emulator -list-avds");
        if (devices.size() == 0) {
            throw new RuntimeException("No devices available for testing");
        }
        return devices;
    }

    public static String getEmulatorPortByName(ArrayList<String> ports, String emulatorName) {
        ArrayList<String> list;
        for (String port : ports) {
            list = RunCommand.runCommand2("adb -s " + port + " emu avd name");
            if (list.get(0).equals(emulatorName)) {
                return list.get(0);
            }
        }
        return "";
    }

    public static String getEmulatorNameByPort(String port) {
        ArrayList<String> list;

        list = RunCommand.runCommand2("adb -s " + port + " emu avd name");
        return list.get(0);
    }
}