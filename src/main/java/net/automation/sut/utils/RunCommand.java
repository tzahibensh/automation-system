package net.automation.sut.utils;

import net.automation.sut.tests.screen.core.managers.ServerManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class RunCommand {

    public static String runCommand(String command) {
        String line;
        ArrayList<String> commandResponse = new ArrayList<>();
        if (command.startsWith("adb")) {
            command = command.replace("adb ", ServerManager.getAndroidHome() + "/platform-tools/adb ");
            Scanner scanner = null;
            try {
                scanner = new Scanner(Runtime.getRuntime().exec(command).getInputStream()).useDelimiter("\\A");
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (scanner.hasNext()) {
                return scanner.next();
            }
        } else {
            try {
                ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
                builder.redirectErrorStream(true);
                Process p = builder.start();
                BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
                while (true) {
                    line = r.readLine();
                    if (line == null) {
                        break;
                    } else {
                        System.out.println(line);
                        commandResponse.add(line);
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }
            if (commandResponse.size() == 0) {
                return "";
            }
            return commandResponse.get(0);
        }
        return "";
    }

    public static ArrayList<String> runCommand2(String command) {
        String line;
        ArrayList<String> commandResponse = new ArrayList<>();

        try {
            ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
            builder.redirectErrorStream(true);
            Process p = builder.start();
            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while (true) {
                line = r.readLine();
                if (line == null) {
                    break;
                } else {
                    System.out.println(line);
                    commandResponse.add(line);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return commandResponse;
    }

}