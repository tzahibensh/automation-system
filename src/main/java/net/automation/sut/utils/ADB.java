package net.automation.sut.utils;

import net.automation.sut.tests.screen.core.Timer;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class ADB {

    private String Id;

    public ADB(String deviceId) {
        this.Id = deviceId;
    }

    public static void startServer() {
        RunCommand.runCommand("adb start-server");
    }

    public static void killServer() {
        RunCommand.runCommand("adb kill-server");
    }

    public static ArrayList<String> getConnectedDevices() {
        ArrayList<String> devices = new ArrayList<>();
        String output = RunCommand.runCommand("adb devices");
        for (String line : output.split("\n")) {
            line = line.trim();
            if (line.endsWith("device")) {
                devices.add(line.replace("device", "").trim());
            }
        }
        return devices;
    }

    public String getForegroundActivity() {
        return RunCommand.runCommand("adb -s " + Id + " shell dumpsys window windows | grep mCurrentFocus");
    }

    public String getAndroidVersionAsString() {
        String output = RunCommand.runCommand("adb -s " + Id + " shell getprop ro.build.version.release");
        output = output.replace("\r\n", "");
        //output = output.substring(0, output.indexOf("\\"));
        if ((output.length() == 3)) {
            output += ".0";
        }
        return output;
    }

    public int getAndroidVersion() {
        String version = getAndroidVersionAsString();
        version = version.replaceAll("\\.", "");
        return Integer.parseInt(version);
    }

    public ArrayList<String> getInstalledPackages() {
        ArrayList<String> packages = new ArrayList<>();
        String[] output = RunCommand.runCommand("adb -s " + Id + " shell pm list packages").split("\n");
        for (String packageId : output) {
            packageId = packageId.replace("packages:", "").trim();
            packages.add(packageId);
        }
        return packages;
    }

    public void openAppsActivity(String packageId, String activityId) {
        RunCommand.runCommand("adb -s " + Id + " shell am start -n " + packageId + "/" + activityId);
    }

    public void clearAppData(String packageId) {
        RunCommand.runCommand("adb -s " + Id + " shell pm clear" + packageId);
    }

    public void forceAppStop(String packageId) {
        RunCommand.runCommand("adb -s " + Id + " shell am force-stop " + packageId);
    }

    public void installApp(String apkPath) {
        RunCommand.runCommand("adb -s " + Id + " install " + apkPath);
    }

    public static void uninstallApp(String device, String packageId) {
        RunCommand.runCommand("adb -s " + device + " uninstall " + packageId);
    }

    public void closeScreen() {
        RunCommand.runCommand("adb -s " + Id + " shell input keyevent 26");
    }

    public void clearLogBuffer() {
        RunCommand.runCommand("adb -s " + Id + " shell -c");
    }

    public void pushFile(String source, String target) {
        RunCommand.runCommand("adb -s " + Id + " push " + source + " " + target);
    }

    public void pullFile(String source, String target) {
        RunCommand.runCommand("adb -s " + Id + " pull " + source + " " + target);
    }

    public void deleteFile(String target) {
        RunCommand.runCommand("adb -s " + Id + " shell rm " + target);
    }

    public void moveFile(String source, String target) {
        RunCommand.runCommand("adb -s " + Id + " shell mv " + source + " " + target);
    }

    public void tackScreenShot(String target) {
        RunCommand.runCommand("adb -s " + Id + " shell screencap " + target);
    }

    public void rebootDevice() {
        RunCommand.runCommand("adb -s " + Id + " reboot");
    }

    public String getDeviceModel() {
        return RunCommand.runCommand("adb -s " + Id + " shell getprop ro.product.model");
    }

    public void getDeviceSerialNumber() {
        RunCommand.runCommand("adb -s " + Id + " shell getprop ro.serialno");
    }

    public void getDeviceCarrier() {
        RunCommand.runCommand("adb -s " + Id + " shell gsm.operator.alpha");
    }

    public void openAppSwitch() {
        RunCommand.runCommand("adb -s " + Id + " shell input keyevent KEYCODE_APP_SWITCH");
    }

    public void showAndroidHome() {
        RunCommand.runCommand("adb -s " + Id + " shell input keyevent 3");
    }

    public boolean isWindowHidden() {
        return RunCommand.runCommand("adb -s " + Id + " shell dumpsys window | grep mState").contains("WINDOW_STATE_HIDDEN");
    }

    public boolean isScreenStateOn() {
        return RunCommand.runCommand("adb -s " + Id + " shell dumpsys window | grep mScreenState").contains("mScreenState=ON");
    }

    public boolean isScreenAwake() {
        return RunCommand.runCommand("adb -s " + Id + " shell dumpsys window | grep \"mWakefulness=\"").contains("mWakefulness=Awake");
    }

    public void unlockDevice() {
        MyLogger.log.info("Unlocking device");
        if (isWindowHidden() || !isScreenStateOn() || !isScreenAwake()) {
            RunCommand.runCommand("adb -s " + Id + " shell input keyevent KEYCODE_POWER");
            RunCommand.runCommand("adb -s " + Id + " shell input keyevent 3");
        } else {
            MyLogger.log.info("Device was already unlocked");
        }
    }

    public String getDeviceName() {
        MyLogger.log.info("Getting device name");
        String deviceName = RunCommand.runCommand("adb -s " + Id + " shell getprop | grep persist.sys.device_name");
        deviceName = deviceName.replace("[persist.sys.device_name]:", "").replace("\r\n", "");
        deviceName = deviceName.replace("[", "").replace("]", "");
        return deviceName.substring(deviceName.indexOf(" "));
    }

    public void selectAppInAppSwitch() {
        RunCommand.runCommand("adb -s " + Id + " shell input keyevent KEYCODE_DPAD_DOWN");
    }

    public void closeAppInAppSwitch() {
        RunCommand.runCommand("adb -s " + Id + " shell input keyevent DEL");
    }

    public ArrayList<String> getLogcatProcesses() {
        String[] output = RunCommand.runCommand("adb -s " + Id + " shell top -n 1 | grep -1 'logcat'").split("\n");
        ArrayList<String> processes = new ArrayList<>();
        for (String line : output) {
            processes.add(line.trim().split(" ")[0]);
            processes.removeAll(Arrays.asList("", null));
        }
        return processes;
    }

    public Object startLogcat(String logId, String grep) {
        ArrayList<String> pidBefore = getLogcatProcesses();
        System.out.println(pidBefore);
        Thread logcat = new Thread(() -> {
            if (grep == null) {
                RunCommand.runCommand("adb -s " + Id + " shell logcat -v threadtime > /sdcard/" + logId + ".txt");
            } else {
                RunCommand.runCommand("adb -s " + Id + " shell logcat -v threadtime | grep -1 '" + grep + "'> /sdcard/" + logId + ".txt");
            }
        });
        logcat.setName(logId);
        logcat.start();
        logcat.interrupt();

        ArrayList<String> pidAfter = getLogcatProcesses();
        System.out.println(pidAfter);
        Timer timer = new Timer();
        timer.start();
        while (!timer.expired(5)) {
            if (pidBefore.size() > 0) {
                pidAfter.removeAll(pidBefore);
            }
            if (pidAfter.size() > 0) {
                break;
            }
            pidAfter = getLogcatProcesses();
        }
        if (pidAfter.size() == 1) {
            return pidAfter.get(0);
        } else if (pidAfter.size() > 1) {
            throw new RuntimeException("multiple logcat processes were started when only one was expected");
        } else {
            throw new RuntimeException("failed to start logcat process");
        }
    }

    public void stopLogcat(Object pid) {
        RunCommand.runCommand("adb -s " + Id + " shell kill " + pid);
    }

    @Test()
    public void test() {
        //System.out.println(command("adb devices"));
        Id = getConnectedDevices().get(0).toString();
        //System.out.println("prior: " + getLogcatProcesses());
        Object pid = startLogcat("1", null);
        System.out.println("pid: " + pid);
        System.out.println("after: " + getLogcatProcesses());
        stopLogcat(pid);
        System.out.println("after stop: " + getLogcatProcesses());
    }
}